MARK ?= not slowtest
KEYWORD ?= test
ARGS ?=

test:
	pytest -m "$(MARK)" -k "$(KEYWORD)" $(ARGS)

slowtest:
	$(MAKE) MARK='slowtest' test

# ----------------------------------------------------------------------------
# The following variables and targets are present to try to clean up and
# recreate the pipeline script in Bitbucket.
# ----------------------------------------------------------------------------

# These variables may be overridden by environment variables with the same name
CLOUDFORMATION_STACK_NAME ?= rhedcloud-aws-rs-account
CLOUDTRAIL_BUCKET_SUFFIX ?= -ct1
CLOUDTRAIL_BUCKET_NAME ?= rhedcloud-pipeline-1$(CLOUDTRAIL_BUCKET_SUFFIX)
CLOUDTRAIL_NAME ?= $(CLOUDFORMATION_STACK_NAME)-Master
RHEDCLOUD_ACCOUNT_NAME ?= RHEDcloud Pipeline 1
REPO_NAME ?= rhedcloud-aws-rs-account-cfn

CHECK_CMD ?= stack-state.py check $(REPO_NAME) \
	./$(REPO_NAME).json > /dev/null 2>&1

# Cache the checksums of each CloudFormation template used for this repository
# so we can determine whether to rebuild the stacks at a later time.
cache-states:
	stack-state.py save $(REPO_NAME) \
		./$(REPO_NAME).json

# Retrieve necessary artifacts and run a sequence of targets to clean and
# rebuild our stacks.
# rebuild: clean rs-account cache-states
rebuild: clean rs-account

# Run a sequence of targets to clean and rebuild our stacks.
local-rebuild: clean rs-account

# Utility to determine whether a stack rebuild is necessary
should-rebuild:
	bash -c "$(CHECK_CMD) && echo No rebuild necessary || echo Rebuild necessary"

# Clean up resources if stack changes are detected
clean:
#	bash -c "$(CHECK_CMD) && $(MAKE) clean-resources || $(MAKE) clean-resources clean-bucket"
	bash -c "$(MAKE) clean-resources clean-bucket"

# Remove resources left behind by previous tests
clean-resources:
	python ./bin/cleanup.py

# Remove the CloudTrail bucket
clean-bucket:
	s3_delete_bucket.py $(CLOUDTRAIL_BUCKET_NAME)

# Create the rs-account CloudFormation stack
rs-account:
# bash -c "$(CHECK_CMD) || cfn_stack_create.py \
# 	--parameter CloudTrailName=$(CLOUDTRAIL_BUCKET_NAME) \
# 	--compact $(CLOUDFORMATION_STACK_NAME) \
# 	./rhedcloud-aws-rs-account-cfn.json"
	bash -c "cfn_stack_create.py \
		--parameter CloudTrailName=$(CLOUDTRAIL_BUCKET_NAME) \
		--compact $(CLOUDFORMATION_STACK_NAME) \
		./rhedcloud-aws-rs-account-cfn.json"


# Upload test artifacts
upload:
	git archive --format zip --output $(REPO_NAME).latest.zip master
	cp $(REPO_NAME).json $(REPO_NAME).latest.json
	cp $(REPO_NAME).latest.zip $(REPO_NAME)-${BITBUCKET_BRANCH}-${BITBUCKET_BUILD_NUMBER}.zip
	upload_artifact.sh $(REPO_NAME).latest.json $(REPO_NAME).latest.zip
	upload_artifact_s3.sh $(REPO_NAME)-${BITBUCKET_BRANCH}-${BITBUCKET_BUILD_NUMBER}.zip rhedcloud-bitbucket-downloads /$(REPO_NAME)/

.EXPORT_ALL_VARIABLES:
