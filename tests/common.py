"""
Common Test Utilities
=====================

Utilities common to tests within the rhedcloud-aws-rs-account-cfn repository.

"""

import os


CLOUDTRAIL_BUCKET_SUFFIX = os.getenv("CLOUDTRAIL_BUCKET_SUFFIX", "-ct1")
