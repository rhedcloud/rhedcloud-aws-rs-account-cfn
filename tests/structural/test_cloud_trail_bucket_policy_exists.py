'''
-----------------
test_cloud_trail_bucket_policy_exists.py
-----------------

"Type": "structural",
"Name": "test_cloud_trail_bucket_policy_exists",
"Description": "Verify that the proper AccountCloudTrailBucketPolicy exists.",
"Plan": "Look up the bucket by suffix and confirm it has a policy.",
"ExpectedResult": "Success"

This test verifies that the Cloud Trail bucket policy exists
by looking for a bucket whose name ends with the expected
bucket name suffix and determining if it has a bucket policy.
'''

from botocore.exceptions import ClientError
import boto3
import pytest

from tests.common import CLOUDTRAIL_BUCKET_SUFFIX


def cloudTrailBucketPolicyExists(bucket_name_suffix):
    try:
        s3r = boto3.resource('s3')
        cloudTrailBucket = None
        for bucket in s3r.buckets.all():
            if bucket.name.endswith(bucket_name_suffix):
                cloudTrailBucket = bucket
        if cloudTrailBucket is not None:
            s3c = boto3.client('s3')
            policy = s3c.get_bucket_policy(Bucket=cloudTrailBucket.name)
            if policy is not None:
                return True
        return False
    except ClientError as err:
        print("Failed to create boto3 resource.\n" + str(err))
        return False


def test_answer():
    assert cloudTrailBucketPolicyExists(CLOUDTRAIL_BUCKET_SUFFIX)
