"""
==========================
test_service_account_group
==========================

These tests verify that the RHEDcloudServiceAccountGroup was created as
expected.

Plan:

* verify that the group exists
* verify group path

${testcount:2}

"""

import boto3
import pytest

from aws_test_functions import get_account_number


@pytest.fixture
def group():
    return boto3.resource("iam").Group("RHEDcloudServiceAccountGroup")


def test_exists(group):
    """Verify that the group's ARN is as expected.

    :param IAM.Group group:
        The Group to check.

    """

    assert group.arn == "arn:aws:iam::{}:group/rhedcloud/RHEDcloudServiceAccountGroup".format(get_account_number())


def test_path(group):
    """Verify the group's path.

    :param IAM.Group group:
        The Group to check.

    """

    assert group.path == "/rhedcloud/"
