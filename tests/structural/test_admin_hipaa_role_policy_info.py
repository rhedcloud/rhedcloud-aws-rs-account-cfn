"""
=================================
test_admin_hipaa_role_policy_info
=================================

These tests verify that the RHEDcloudAdministratorRoleHipaaPolicy was created
as expected.

Plan:

* verify that the policy exists
* verify the roles that are attached to the policy
* verify the groups that are attached to the policy

${testcount:3}

"""

import boto3
import pytest

from aws_test_functions import get_account_number


@pytest.fixture
def policy():
    arn = "arn:aws:iam::{}:policy/rhedcloud/RHEDcloudAdministratorRoleHipaaPolicy".format(get_account_number())
    return boto3.resource("iam").Policy(arn)


def test_exists(policy):
    """Verify that the policy's name is as expected.

    :param IAM.Policy policy:
        The Policy to check.

    """

    assert policy.policy_name == "RHEDcloudAdministratorRoleHipaaPolicy"


def test_attached_roles(policy):
    """Verify that the policy has certain roles attached to it.

    :param IAM.Policy policy:
        The Policy to check.

    """

    roles = [r.name for r in policy.attached_roles.all()]
    assert len(roles) == 1
    assert "RHEDcloudAdministratorRole" in roles


def test_attached_groups(policy):
    """Verify that the policy has certain groups attached to it.

    :param IAM.Policy policy:
        The Policy to check.

    """

    groups = [g.name for g in policy.attached_groups.all()]
    assert len(groups) == 1
    assert "RHEDcloudServiceAccountGroup" in groups
