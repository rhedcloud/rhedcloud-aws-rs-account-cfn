'''
-----------------
test_cloud_trail_bucket_policy_document.py
-----------------

"Type": "structural",
"Name": "test_cloud_trail_bucket_policy_document",
"Description": "Verify that the proper policy appears in the policy document.",
"Plan": "Look up the bucket by policy, inspect the policy, and compare the policy document.",
"ExpectedResult": "Success"

This test verifies that the proper policy appears in the policy document
by comparing the deployed bucket policy with the expected bucket policy.
PNP
'''

import json

import boto3
import pytest

from aws_test_functions import *
from tests.common import CLOUDTRAIL_BUCKET_SUFFIX


def getBucketPolicyDocument(bucket_name_suffix):
    try:
        cloudTrailBucket = getBucketNameFromSuffix(bucket_name_suffix)

        if cloudTrailBucket != None:
            s3c = boto3.client('s3')
            policy = s3c.get_bucket_policy(Bucket=cloudTrailBucket)
            if policy != None:
                policyDocument = json.loads(policy['Policy'])
                policyDocument = json.dumps(policyDocument)
                return policyDocument
        return False
    except ClientError as err:
        print("Failed to create boto3 resource.\n" + str(err))
        return False

def getBucketNameFromSuffix(bucket_name_suffix):
    try:
        s3 = boto3.resource('s3')
        for bucket in s3.buckets.all():
            if bucket.name.endswith(bucket_name_suffix):
                cloudTrailBucket = bucket
                return bucket.name
        return False
    except ClientError as err:
        print("Failed to create boto3 resource.\n" + str(err))
        return False

def test_answer():


    resource1 = "arn:aws:s3:::" + getBucketNameFromSuffix(CLOUDTRAIL_BUCKET_SUFFIX)
    resource2 = resource1 + "/AWSLogs/" + get_account_number() + "/*"

    # Bucket Policy
    bucket_policy = {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Sid": "AWSCloudTrailAclCheck",
                "Effect": "Allow",
                "Principal": {
                    "Service": "cloudtrail.amazonaws.com"
                },
                "Action": "s3:GetBucketAcl",
                "Resource": resource1
            },
            {
                "Sid": "AWSCloudTrailWrite",
                "Effect": "Allow",
                "Principal": {
                    "Service": "cloudtrail.amazonaws.com"
                },
                "Action": "s3:PutObject",
                "Resource": resource2,
                "Condition": {
                    "StringEquals": {
                        "s3:x-amz-acl": "bucket-owner-full-control"
                    }
                }
             }
        ]
    }

    # Convert policy to a JSON string
    bucket_policy = json.dumps(bucket_policy)

    assert getBucketPolicyDocument(CLOUDTRAIL_BUCKET_SUFFIX) == bucket_policy
