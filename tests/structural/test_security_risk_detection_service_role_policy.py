"""
================================================
test_security_risk_detection_service_role_policy
================================================

These tests verify that the RHEDcloudSecurityRiskDetectionServiceRolePolicy was
created as expected.

Plan:

* verify that the policy exists
* verify the policy path
* verify the roles that are attached to the policy

${testcount:3}

"""

import boto3
import pytest

from aws_test_functions import get_account_number


@pytest.fixture
def policy():
    arn = "arn:aws:iam::{}:policy/rhedcloud/RHEDcloudSecurityRiskDetectionServiceRolePolicy".format(get_account_number())
    return boto3.resource("iam").Policy(arn)


def test_exists(policy):
    """Verify that the policy's name is as expected.

    :param IAM.Policy policy:
        The Policy to check.

    """

    assert policy.policy_name == "RHEDcloudSecurityRiskDetectionServiceRolePolicy"


def test_policy_path(policy):
    """Verify the policy's path.

    :param IAM.Policy policy:
        The Policy to check.

    """

    assert policy.path == "/rhedcloud/"


def test_attached_roles(policy):
    """Verify that the policy has certain roles attached to it.

    :param IAM.Policy policy:
        The Policy to check.

    """

    roles = [r.name for r in policy.attached_roles.all()]
    assert len(roles) == 1
    assert "RHEDcloudSecurityRiskDetectionServiceRole" in roles
