'''
This test verifies that the RHEDcloudAccountCloudTrailPolicy  was created
by querying for it by name and verifying that its ARN is
correct, ignoring the account number as this may run in
various accounts.  TJ
'''
import boto3
from aws_test_functions import *

def getPolicyName(policy_arn):
    try:
        iam = boto3.resource('iam')
        policy = iam.Policy(policy_arn)
        return policy.policy_name
    except ClientError as err:
        print("Failed to create boto3 resource.\n" + str(err))
        return False

def test_answer():
    assert (getPolicyName('arn:aws:iam::' + get_account_number() + ':policy/RHEDcloudAccountCloudTrailPolicy')) == "RHEDcloudAccountCloudTrailPolicy" 
