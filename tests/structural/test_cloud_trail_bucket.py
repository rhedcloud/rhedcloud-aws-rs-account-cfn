"""
=======================
test_cloud_trail_bucket
=======================

These tests verify that the CloudTrail S3 bucket was created as
expected.

Plan:

* verify that the bucket exists
* verify that the bucket has a lifecycle policy

${testcount:2}

"""

import boto3
import pytest

from aws_test_functions import get_account_number

from tests.common import CLOUDTRAIL_BUCKET_SUFFIX


@pytest.fixture
def bucket():
    s3 = boto3.resource("s3")
    for bucket in s3.buckets.all():
        if not bucket.name.endswith(CLOUDTRAIL_BUCKET_SUFFIX):
            continue

        return bucket


def test_exists(bucket):
    """Verify that the bucket exists.

    :param S3.Bucket bucket:
        The Bucket to check.

    """

    assert bucket is not None


def test_lifecycle(bucket):
    """Verify that the bucket has the expected lifecycle policy.

    :param S3.Bucket bucket:
        The Bucket to check.

    """

    lifecycle = bucket.Lifecycle()
    assert dict(
        ID="DeleteSvcAccountCredentialsAfterThirtyDays",
        Expiration={
            "Days": 30,
        },
        Status="Enabled",
    ) in lifecycle.rules
