"""
=============================
test_aws_account_service_role
=============================

These tests verify that the RHEDcloudAwsAccountServiceRole was created as
expected.

Plan:

* verify that the role exists
* verify role path
* verify the policies attached to the role
* verify the AssumeRolePolicyDocument

${testcount:4}

"""

import boto3
import pytest

from aws_test_functions import get_account_number


@pytest.fixture
def role():
    return boto3.resource("iam").Role("RHEDcloudAwsAccountServiceRole")


def test_exists(role):
    """Verify that the role's ARN is as expected.

    :param IAM.Role role:
        The Role to check.

    """

    assert role.arn == "arn:aws:iam::{}:role/rhedcloud/RHEDcloudAwsAccountServiceRole".format(get_account_number())


def test_role_path(role):
    """Verify the role's path.

    :param IAM.Role role:
        The Role to check.

    """

    assert role.path == "/rhedcloud/"


def test_attached_policies(role):
    """Verify that the role has certain policies attached to it.

    :param IAM.Role role:
        The Role to check.

    """

    policies = [p.policy_name for p in role.attached_policies.all()]
    assert len(policies) == 1
    assert "RHEDcloudAwsAccountServiceRolePolicy" in policies


def test_policy_document(role):
    """Verify the role's AssumeRolePolicyDocument.

    :param IAM.Role role:
        The Role to check.

    """

    assert role.assume_role_policy_document == {
        'Version': '2012-10-17',
        'Statement': [{
            'Action': 'sts:AssumeRole',
            'Effect': 'Allow',
            'Principal': {
                'AWS': 'arn:aws:iam::410404204135:user/RHEDcloudAwsAccountService'
            }
        }],
    }
