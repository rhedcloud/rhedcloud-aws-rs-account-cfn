"""
======================
test_centra_admin_role
======================

These tests verify that the RHEDcloudCentralAdministratorRole role was created
as expected.

Plan:

* verify that the role exists
* verify max session duration
* verify role path
* verify that the AdministratorAccess Managed Policy is attached

${testcount:4}

"""

import boto3
import pytest

from aws_test_functions import get_account_number


@pytest.fixture
def role():
    return boto3.resource("iam").Role("RHEDcloudCentralAdministratorRole")


def test_exists(role):
    """Verify that the role's ARN is as expected.

    :param IAM.Role role:
        The Role to check.

    """

    assert role.arn == "arn:aws:iam::{}:role/rhedcloud/RHEDcloudCentralAdministratorRole".format(get_account_number())


def test_max_session_duration(role):
    """Verify the role's maximum session duration.

    :param IAM.Role role:
        The Role to check.

    """

    assert role.max_session_duration == 43200


def test_role_path(role):
    """Verify the role's path.

    :param IAM.Role role:
        The Role to check.

    """

    assert role.path == "/rhedcloud/"


def test_attached_policies(role):
    """Verify that the role has certain policies attached to it.

    :param IAM.Role role:
        The Role to check.

    """

    policies = [p.policy_name for p in role.attached_policies.all()]
    assert len(policies) == 1
    assert "AdministratorAccess" in policies
