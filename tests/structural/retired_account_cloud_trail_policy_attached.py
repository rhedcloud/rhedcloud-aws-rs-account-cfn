import boto3
from aws_test_functions import *

'''
----------------------------------
test_account_cloud_trail_policy_attached.py
----------------------------------
This test verifies that the RHEDcloudAccountCloudTrailPolicy is attached
to the following roles:
   RHEDcloudAdministratorRole
by verifying that the correct policy ARN is returned when
querying for attached policies.
'''

# -----------------------------------

def getAccountCloudTrailPolicyAttached():
    try:
        matched = 0

        iam = boto3.resource('iam')

        rolesWithPolicy = [
                           "RHEDcloudAdministratorRole"
                          ]
        
        rolesWithPolicyCount = len(rolesWithPolicy)

        for roleWithPolicy in rolesWithPolicy:
            role = iam.Role(roleWithPolicy)

            policies = role.attached_policies

            for policy in policies.all():
                if policy.arn == "arn:aws:iam::" + get_account_number() + ":policy/RHEDcloudAccountCloudTrailPolicy":
                   matched = matched + 1

        if matched == rolesWithPolicyCount:
           return True
        return False

    except ClientError as err:
        print("Failed to create boto3 resource.\n" + str(err))
        return False

def test_answer():
    assert getAccountCloudTrailPolicyAttached() == True

# -----------------------------------
