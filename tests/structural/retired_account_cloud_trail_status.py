'''
----------------------------------
test_account_cloud_trail_status.py
----------------------------------

"Type": "structural",
"Name": "test_account_cloud_trail_status",
"Description": "Verify Cloud Trail logging to correct S3 bucket and check to make sure logging is enabled.",
"Plan": "Lookup s3 bucket ending in '-ct1' and compare it to the s3 bucket defined in the Cloud Trail configuration.  Then check logging status.",
"ExpectedResult": "Success"

This test verifies that Cloud Trail is writing to correct S3 bucket by:

- determine S3 Bucket name ending in '-ct1'
- determine Bucket name used by CloudTrail
- determine if both bucket names agree
- determine if cloudtrail is logging
- failing if any discrepancy

'''

import boto3
from aws_test_functions import ClientError
from tests.common import CLOUDTRAIL_BUCKET_SUFFIX

# -----------------------------------

def getCloudTrailStatus(bucket_name_suffix):
    try:
        # ------------------------------------------
        # determine S3 Bucket name ending in '-ct1'
        # ------------------------------------------

        s3r = boto3.resource('s3')
        cloudTrailBucket = None

        for bucket in s3r.buckets.all():
            if bucket.name.endswith(bucket_name_suffix):
               cloudTrailBucket = bucket.name

        if cloudTrailBucket != None:
           s3c = boto3.client('s3')
        else:
           print("*** cloudTrailBucket:",cloudTrailBucket)
           return False

        # - - - - -

        # -----------------------------------------
        # determine Bucket name used by CloudTrail
        # -----------------------------------------

        cloudtrail = boto3.client('cloudtrail')
        cdt = cloudtrail.describe_trails(trailNameList=[],
                                         includeShadowTrails=False)

        x = cdt["trailList"]
        trail = x[0]

        S3BucketName = trail["S3BucketName"]

        # - - - - -

        # -------------------------------------
        # determine if both bucket names agree
        # -------------------------------------

        if S3BucketName != cloudTrailBucket:
           print("*** S3BucketName:",S3BucketName)
           print("*** cloudTrailBucket:",cloudTrailBucket)
           print("*** Returning: FALSE !!")
           return False

        # - - - - -

        # -----------------------------------
        # determine if CloudTrail is logging
        # ----------------------------------

        TrailName = trail["Name"]
        cgts = cloudtrail.get_trail_status(Name=TrailName)

        y = cgts["IsLogging"]
        print("*** IsLogging:", y)
        return y

    except ClientError as err:
        print("Failed to create boto3 resource.\n" + str(err))
        return False

def test_answer():
    assert getCloudTrailStatus(CLOUDTRAIL_BUCKET_SUFFIX) == True

# -----------------------------------
