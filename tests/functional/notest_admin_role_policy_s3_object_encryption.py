'''
-----------------
test_admin_role_policy_s3_object_encryption.py
-----------------

Type": "functional",
Name": "test_admin_role_policy_s3_object_encryption",
Description": "Verify that the RHEDcloudDenyIncorrectEncryptionHeader and RHEDcloudDenyUnEncryptedObjectUploads statements within the RHEDcloudAdministratorRolePolicy policy is performing as expected against the specified resources.",
Plan": "Attach *:* and this policy, test each statement and expect AccessDenied errors for each blocked call.",
ExpectedResult": "Failure"

This test verifies that the RHEDcloudDenyIncorrectEncryptionHeader and RHEDcloudDenyUnEncryptedObjectUploads statements
within the RHEDcloudAdministratorRolePolicy are disallowing users from performing the following actions on the
appropriate resources and allowing on other resources:

s3:putObject


${testcount:4}

'''

import boto3
from aws_test_functions import *
from botocore.exceptions import ClientError
from datetime import datetime
from time import sleep
import json
from random import randint
import os

import pprint

def failed_test(resource, reason):
    print(resource, "\n", reason, "\n")
    assert False

def passed_test(resource, reason):
    print(resource, "\n", reason, "\n")
    assert True

def admin_delete_bucket(s3_bucket_name):
    empty_bucket(s3_bucket_name)
    sleep(2)
    delete_bucket(s3_bucket_name)
    print("Deleted S3 Bucket: " + s3_bucket_name)


def test_answer():
    # s3_bucket_name = "serviceforge-aws-admin-105-ct1"
    # trail_name = 'rhedcloud-aws-rs-account-Master'
    s3_bucket_name = os.environ['CLOUDTRAIL_BUCKET_NAME']
    trail_name = os.environ['CLOUDTRAIL_NAME']
    user = create_test_user('test_s3_object_encryption_statement')
    rhedcloudAdminRolePolicyArn = "arn:aws:iam::"+get_account_number()+":policy/rhedcloud/RHEDcloudAdministratorRolePolicy"
    attach_policy_to_user(user['UserName'],rhedcloudAdminRolePolicyArn)
    attach_policy_to_user(user['UserName'],"arn:aws:iam::aws:policy/AdministratorAccess")
    session = create_session_for_test_user(user['UserName'])
    user_iam = session.client('iam')
    admin_iam = boto3.client('iam')
    user_s3 = session.client('s3')
    admin_s3 = boto3.client('s3')
    s3_logging_bucket = "arn:aws:s3:::" + s3_bucket_name

    # Variables
    creation_s3_bucket_name = "test_s3_object_encryption_statement" + str(randint(0, 100000))
    creation_s3_bucket_name = creation_s3_bucket_name.replace('_', '-')[:63]
    creation_s3_bucket = None

    creation_s3_bucket2_name = "elasticbeanstalk-us-east-1-" + str(get_account_number())
    creation_s3_bucket2_name = creation_s3_bucket2_name.replace('_', '-')[:63]
    creation_s3_bucket2 = None

    # Policies

    try:

        # Prep
        response = admin_s3.create_bucket(
            Bucket=creation_s3_bucket_name
        )
        creation_s3_bucket = "true"

        response = admin_s3.create_bucket(
            Bucket=creation_s3_bucket2_name
        )
        creation_s3_bucket2 = "true"

        sleep(10)


        # s3:putObject
        try:
            response = user_s3.put_object(
                Body=b'testing',
                Bucket=creation_s3_bucket_name,
                Key='testnonencryp.txt'
            )


            failed_test('Error', 's3:putObject')
        except ClientError as e:
            if e.response['Error']['Code'] == 'AccessDenied':
                print("Expected error, test successful.")
            elif e.response['Error']['Code'] == 'AuthorizationError':
                print("Expected error, test successful.")
            else:
                failed_test('Unknown Error', 's3:putObject: ' + str(e))

        # s3:putObject
        try:
            response = user_s3.put_object(
                Body=b'testing',
                Bucket=creation_s3_bucket_name,
                Key='testkmsencryp.txt',
                ServerSideEncryption='aws:kms',
            )


            failed_test('Error', 's3:putObject')
        except ClientError as e:
            if e.response['Error']['Code'] == 'AccessDenied':
                print("Expected error, test successful.")
            elif e.response['Error']['Code'] == 'AuthorizationError':
                print("Expected error, test successful.")
            else:
                failed_test('Unknown Error', 's3:putObject: ' + str(e))


        # s3:putObject Encrypted
        try:
            response = user_s3.put_object(
                Body=b'testing',
                Bucket=creation_s3_bucket_name,
                Key='testencryp.txt',
                ServerSideEncryption='AES256'
            )
        except ClientError as e:
            if e.response['Error']['Code'] == 'AccessDenied':
                failed_test('Permissions', 's3:putObject')
            elif e.response['Error']['Code'] == 'AuthorizationError':
                failed_test('Permissions', 's3:putObject')
            else:
                failed_test('Unknown Error', 's3:putObject: ' + str(e))

        # s3:putObject beanstalk
        try:
            response = user_s3.put_object(
                Body=b'testing',
                Bucket=creation_s3_bucket2_name,
                Key='test.txt'
            )
        except ClientError as e:
            if e.response['Error']['Code'] == 'AccessDenied':
                failed_test('Permissions', 's3:putObject')
            elif e.response['Error']['Code'] == 'AuthorizationError':
                failed_test('Permissions', 's3:putObject')
            else:
                failed_test('Unknown Error', 's3:putObject: ' + str(e))


        passed_test("Passed", "Passed")
    finally:
        print("Finally Clean Up")

        if creation_s3_bucket:
            admin_delete_bucket(creation_s3_bucket_name)
        if creation_s3_bucket2:
            admin_delete_bucket(creation_s3_bucket2_name)


        delete_test_user(user['UserName'])
        print("Finish Clean Up")
