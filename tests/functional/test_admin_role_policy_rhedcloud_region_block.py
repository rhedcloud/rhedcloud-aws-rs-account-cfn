'''
=============================================
test_admin_role_policy_rhedcloud_region_block
=============================================

Type": "functional",
Name": "test_admin_role_policy_rhedcloud_region_block",
Description": "Verify that the RHEDcloudRegionBlock statement within the RHEDcloudAdministratorRolePolicy policy is performing as expected against the specified resources.",
Plan": "Attach *:* and this policy, test each statement and expect AccessDenied errors for each blocked call.",
ExpectedResult": "Failure"

This test verifies that the RHEDcloudRegionBlock statement within the RHEDcloudAdministratorRolePolicy are disallowing users from performing the following actions on the
appropriate resources and allowing on other resources:

*:*


${testcount:5}

'''

import boto3
import pytest

from aws_test_functions import (
    aws_client,
    debug,
    has_status,
    ignore_errors,
    make_identifier,
    unexpected,
)


# This allows us to run all tests for each region without duplicating code.
pytestmark = pytest.mark.parametrize("region", (
    "us-east-1",
    "us-east-2",
    "us-west-1",
    "us-west-2",

    # this region is expected to fail with an AccessDenied error
    pytest.param(
        "ca-central-1",
        marks=pytest.mark.raises_access_denied,
    ),
))


@pytest.fixture
def sqs(session, region):
    yield session.client("sqs", region)


@aws_client("sqs")
def delete_queue(url, *, sqs):
    """Delete a queue.

    :param str url:
        A queue url.

    """

    debug("Deleting queue: {}".format(url))
    res = sqs.delete_queue(QueueUrl=url)
    assert has_status(res, 200), unexpected(res)


def test_create_queue(sqs, region, stack):
    """Verify access to create new SQS queues.

    :param SQS.Client sqs:
        Handle to the SQS API.
    :param str region:
        Name of an AWS region.
    :param contextlib.ExitStack stack:
        An existing context manager that will help clean up after ourselves.

    """

    name = make_identifier("test")
    res = sqs.create_queue(QueueName=name)
    assert has_status(res, 200), unexpected(res)

    url = res["QueueUrl"]
    debug("Created queue: {}".format(url))

    stack.callback(
        ignore_errors(delete_queue),
        url,
        sqs=boto3.client("sqs", region),
    )
