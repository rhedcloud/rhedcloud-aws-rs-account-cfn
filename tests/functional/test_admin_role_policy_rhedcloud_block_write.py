"""
============================================
test_admin_role_policy_rhedcloud_block_write
============================================

"Type": "functional",
"Name": "test_admin_role_policy_rhedcloud_block_write",
"Description": "Verify that the rhedcloudBlockWrite statement within the RHEDcloudAdministratorRolePolicy policy is performing as expected against the specified resources.",
"Plan": "Attach *:* and this policy, test each statement and expect AccessDenied errors for each blocked call.",
"ExpectedResult": "Failure"

This test verifies that the rhedcloudBlockWrite statement is
disallowing users from performing the following actions on the
appropriate resources and allowing on other resources:

"iam:Attach",
"iam:Detach",
"iam:Update",
"iam:Create",
"iam:Delete",
"glacier:Create",
"glacier:Upload",
"glacier:Delete",
"cloudtrail:CreateTrail",
"cloudtrail:AddTags",
"cloudtrail:UpdateTrail"
"cloudtrail:DeleteTrail",
"cloudtrail:GetEventSelectors",
"cloudtrail:GetTrailStatus",
"cloudtrail:PutEventSelectors",
"cloudtrail:RemoveTags",
"cloudtrail:StartLogging",
"cloudtrail:StopLogging",
"cloudformation:CreateStack",
"cloudformation:UpdateStack",
"cloudformation:DeleteStack",
"logs:Delete",
"logs:Put",
"logs:Create",
"events:Delete",
"events:Enable",
"events:Disable",
"events:Put"


${testcount:54}

"""

import json

import pytest

from aws_test_functions import (
    debug,
    has_status,
    ignore_errors,
    unexpected,
)


@pytest.dict_fixture
def shared_vars():
    return {
        "policy_arn": None,
    }


##############################################################################
#                                IAM TESTS                                   #
##############################################################################


@pytest.fixture
def iam(factory):
    return factory.client("iam")


@pytest.fixture
def iam_name():
    return "test_rhedcloud_block_write_statement"


@pytest.admin_only
def test_create_policy(iam, iam_name, stack, shared_vars):
    """Verify access to create IAM policies as an admin.

    :param IAM.Client iam:
        Handle to the IAM API.
    :param str iam_name:
        Name of the policy to create.
    :param contextlib.ExitStack stack:
        An existing context manager that will help clean up after ourselves.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    res = iam.create_policy(
        PolicyName=iam_name,
        Path="/rhedcloud/",
        PolicyDocument=json.dumps({
            "Version": "2012-10-17",
            "Statement": [{
                "Effect": "Allow",
                "Action": "ec2:Describe*",
                "Resource": "*",
            }]
        }),
        Description=iam_name,
    )
    assert has_status(res, 200), unexpected(res)

    shared_vars["policy_arn"] = arn = res["Policy"]["Arn"]
    debug("Created IAM Policy: {}".format(arn))

    stack.callback(
        ignore_errors(test_delete_policy),
        iam,
        arn,
    )


@pytest.admin_only
def test_create_user(iam, iam_name, stack):
    """Verify access to create IAM users.

    :param IAM.Client iam:
        Handle to the IAM API.
    :param str iam_name:
        Name of the user to create.
    :param contextlib.ExitStack stack:
        An existing context manager that will help clean up after ourselves.

    """

    res = iam.create_user(
        UserName=iam_name,
        Path="/rhedcloud/",
    )
    assert has_status(res, 200), unexpected(res)

    debug("Created user: {}".format(iam_name))
    stack.callback(
        ignore_errors(test_delete_user),
        iam,
        iam_name,
    )


@pytest.admin_only
def test_attach_user_policy(iam, iam_name, policy_arn, stack):
    """Verify access to attach IAM policies to IAM users.

    :param IAM.Client iam:
        Handle to the IAM API.
    :param str iam_name:
        Name of an IAM user.
    :param str policy_arn:
        ARN of an IAM policy.
    :param contextlib.ExitStack stack:
        An existing context manager that will help clean up after ourselves.

    """

    res = iam.attach_user_policy(
        UserName=iam_name,
        PolicyArn=policy_arn,
    )
    assert has_status(res, 200), unexpected(res)

    debug("Attached policy {} to user {}".format(policy_arn, iam_name))
    stack.callback(
        ignore_errors(test_detach_user_policy),
        iam,
        iam_name,
        policy_arn,
    )


@pytest.admin_only
def test_create_role(iam, iam_name, stack):
    """Verify access to create IAM roles.

    :param IAM.Client iam:
        Handle to the IAM API.
    :param str iam_name:
        Name of the role to create.
    :param contextlib.ExitStack stack:
        An existing context manager that will help clean up after ourselves.

    """

    res = iam.create_role(
        RoleName=iam_name,
        Path="/rhedcloud/",
        AssumeRolePolicyDocument=json.dumps({
            "Version": "2012-10-17",
            "Statement": [{
                "Effect": "Allow",
                "Principal": {
                    "Service": ["lambda.amazonaws.com"],
                },
                "Action": "sts:AssumeRole"
            }],
        }),
        Description=iam_name,
    )
    assert has_status(res, 200), unexpected(res)

    debug("Created role: {}".format(iam_name))
    stack.callback(
        ignore_errors(test_delete_role),
        iam,
        iam_name,
    )


@pytest.admin_only
def test_attach_role_policy(iam, iam_name, policy_arn, stack):
    """Verify access to attach IAM policies to IAM roles.

    :param IAM.Client iam:
        Handle to the IAM API.
    :param str iam_name:
        Name of an IAM role.
    :param str policy_arn:
        ARN of an IAM policy.
    :param contextlib.ExitStack stack:
        An existing context manager that will help clean up after ourselves.

    """

    res = iam.attach_role_policy(
        RoleName=iam_name,
        PolicyArn=policy_arn,
    )
    assert has_status(res, 200), unexpected(res)

    debug("Attached policy {} to role {}".format(policy_arn, iam_name))
    stack.callback(
        ignore_errors(test_detach_role_policy),
        iam,
        iam_name,
        policy_arn,
    )


@pytest.admin_only
def test_create_group(iam, iam_name, stack):
    """Verify access to create IAM groups.

    :param IAM.Client iam:
        Handle to the IAM API.
    :param str iam_name:
        Name of the group to create.
    :param contextlib.ExitStack stack:
        An existing context manager that will help clean up after ourselves.

    """

    res = iam.create_group(
        GroupName=iam_name,
        Path="/rhedcloud/",
    )
    assert has_status(res, 200), unexpected(res)

    debug("Created IAM Group: {}".format(iam_name))
    stack.callback(
        ignore_errors(test_delete_group),
        iam,
        iam_name,
    )


@pytest.admin_only
def test_attach_group_policy(iam, iam_name, policy_arn, stack):
    """Verify access to attach IAM policies to IAM groups.

    :param IAM.Client iam:
        Handle to the IAM API.
    :param str iam_name:
        Name of an IAM group.
    :param str policy_arn:
        ARN of an IAM policy.
    :param contextlib.ExitStack stack:
        An existing context manager that will help clean up after ourselves.

    """

    res = iam.attach_group_policy(
        GroupName=iam_name,
        PolicyArn=policy_arn,
    )
    assert has_status(res, 200), unexpected(res)

    debug("Attached policy {} to group {}".format(policy_arn, iam_name))
    stack.callback(
        ignore_errors(test_detach_group_policy),
        iam,
        iam_name,
        policy_arn,
    )


@pytest.mark.skip("moved to scp: test_iam_users.test_attach_user_policy")
@pytest.blocked_user_only
def test_attach_user_policy_as_user(iam, iam_name, policy_arn, stack):
    test_attach_user_policy(iam, iam_name, policy_arn, stack)


@pytest.mark.skip("moved to scp: test_iam_roles.test_attach_role_policy")
@pytest.blocked_user_only
def test_attach_role_policy_as_user(iam, iam_name, policy_arn, stack):
    test_attach_role_policy(iam, iam_name, policy_arn, stack)


@pytest.mark.skip("moved to scp: test_iam_groups.test_attach_group_policy")
@pytest.blocked_user_only
def test_attach_group_policy_as_user(iam, iam_name, policy_arn, stack):
    test_attach_group_policy(iam, iam_name, policy_arn, stack)


@pytest.mark.skip("moved to scp: test_iam_users.test_detach_user_policy")
@pytest.blocked_user_only
def test_detach_user_policy(iam, iam_name, policy_arn):
    """Verify access to detach IAM policies from IAM users.

    :param IAM.Client iam:
        Handle to the IAM API.
    :param str iam_name:
        Name of an IAM user.
    :param str policy_arn:
        ARN of an IAM policy.

    """

    debug("Detaching policy {} from user {}".format(policy_arn, iam_name))
    res = iam.detach_user_policy(
        UserName=iam_name,
        PolicyArn=policy_arn,
    )
    assert has_status(res, 200), unexpected(res)


@pytest.mark.skip("moved to scp: test_iam_roles.test_detach_role_policy")
@pytest.blocked_user_only
def test_detach_role_policy(iam, iam_name, policy_arn):
    """Verify access to detach IAM policies from IAM roles.

    :param IAM.Client iam:
        Handle to the IAM API.
    :param str iam_name:
        Name of an IAM role.
    :param str policy_arn:
        ARN of an IAM policy.

    """

    debug("Detaching policy {} from role {}".format(policy_arn, iam_name))
    res = iam.detach_role_policy(
        RoleName=iam_name,
        PolicyArn=policy_arn,
    )
    assert has_status(res, 200), unexpected(res)


@pytest.mark.skip("moved to scp: test_iam_groups.test_detach_group_policy")
@pytest.blocked_user_only
def test_detach_group_policy(iam, iam_name, policy_arn):
    """Verify access to detach IAM policies from IAM groups.

    :param IAM.Client iam:
        Handle to the IAM API.
    :param str iam_name:
        Name of an IAM group.
    :param str policy_arn:
        ARN of an IAM policy.

    """

    debug("Detaching policy {} from group {}".format(policy_arn, iam_name))
    res = iam.detach_group_policy(
        GroupName=iam_name,
        PolicyArn=policy_arn,
    )
    assert has_status(res, 200), unexpected(res)


@pytest.mark.skip("moved to scp: test_iam_users.test_modify_user")
@pytest.blocked_user_only
def test_update_user(iam, iam_name):
    """Verify access to update IAM users.

    :param IAM.Client iam:
        Handle to the IAM API.
    :param str iam_name:
        Name of an IAM user.

    """

    debug("Updating user: {}".format(iam_name))
    res = iam.update_user(
        UserName=iam_name,
        NewUserName=iam_name,
    )
    assert has_status(res, 200), unexpected(res)


@pytest.mark.skip("moved to scp: test_iam_roles.test_update_role")
@pytest.blocked_user_only
def test_update_role(iam, iam_name):
    """Verify access to update IAM roles.

    :param IAM.Client iam:
        Handle to the IAM API.
    :param str iam_name:
        Name of an IAM role.

    """

    debug("Updating role: {}".format(iam_name))
    res = iam.update_role(
        RoleName=iam_name,
        Description="string",
    )
    assert has_status(res, 200), unexpected(res)


@pytest.mark.skip("moved to scp: test_iam_groups.test_modify_iam_group")
@pytest.blocked_user_only
def test_update_group(iam, iam_name):
    """Verify access to update IAM groups.

    :param IAM.Client iam:
        Handle to the IAM API.
    :param str iam_name:
        Name of an IAM group.

    """

    debug("Updating group: {}".format(iam_name))
    res = iam.update_group(
        GroupName=iam_name,
        NewGroupName=iam_name,
    )
    assert has_status(res, 200), unexpected(res)


@pytest.mark.skip("scp: test_iam_users.test_create_rhedcloud_user")
@pytest.blocked_user_only
def test_create_user_as_user(iam, iam_name, stack):
    test_create_user(iam, iam_name, stack)


@pytest.mark.skip("scp: test_iam_roles.test_create_rhedcloud_role")
@pytest.blocked_user_only
def test_create_role_as_user(iam, iam_name, stack):
    test_create_role(iam, iam_name, stack)


@pytest.mark.skip("scp: test_iam_groups.test_create_iam_group")
@pytest.blocked_user_only
def test_create_group_as_user(iam, iam_name, stack):
    test_create_group(iam, iam_name, stack)


@pytest.mark.skip("scp: test_iam_policies.test_create_rhedcloud_policy")
@pytest.blocked_user_only
def test_create_policy_as_user(iam, iam_name, stack, shared_vars):
    test_create_policy(iam, iam_name, stack, shared_vars)


@pytest.mark.skip("scp: test_iam_users.test_delete_rhedcloud_user")
@pytest.blocked_user_only
def test_delete_user(iam, iam_name):
    """Verify access to delete IAM users.

    :param IAM.Client iam:
        Handle to the IAM API.
    :param str iam_name:
        Name of an IAM user.

    """

    debug("Deleting user: {}".format(iam_name))
    res = iam.delete_user(
        UserName=iam_name,
    )
    assert has_status(res, 200), unexpected(res)


@pytest.mark.skip("scp: test_iam_roles.test_delete_rhedcloud_role")
@pytest.blocked_user_only
def test_delete_role(iam, iam_name):
    """Verify access to delete IAM roles.

    :param IAM.Client iam:
        Handle to the IAM API.
    :param str iam_name:
        Name of an IAM role.

    """

    debug("Deleting role: {}".format(iam_name))
    res = iam.delete_role(
        RoleName=iam_name,
    )
    assert has_status(res, 200), unexpected(res)


@pytest.mark.skip("scp: test_iam_groups.test_delete_iam_group")
@pytest.blocked_user_only
def test_delete_group(iam, iam_name):
    """Verify access to delete IAM groups.

    :param IAM.Client iam:
        Handle to the IAM API.
    :param str iam_name:
        Name of an IAM group.

    """

    debug("Deleting group: {}".format(iam_name))
    res = iam.delete_group(
        GroupName=iam_name,
    )
    assert has_status(res, 200), unexpected(res)


@pytest.mark.skip("scp: test_iam_policies.test_delete_rhedcloud_policy")
@pytest.blocked_user_only
def test_delete_policy(iam, policy_arn):
    """Verify access to delete IAM policies.

    :param IAM.Client iam:
        Handle to the IAM API.
    :param str policy_arn:
        ARN of an IAM policy.

    """

    debug("Deleting IAM Policy: {}".format(iam_name))
    res = iam.delete_policy(
        PolicyArn=policy_arn,
    )
    assert has_status(res, 200), unexpected(res)


##############################################################################
#                          CLOUDFORMATION TESTS                              #
##############################################################################


@pytest.fixture
def cloudformation(factory):
    return factory.client("cloudformation")


@pytest.fixture
def stack_name():
    return "rhedcloudcreationteststack"


# @pytest.admin_and_blocked_user
@pytest.admin_only
def test_create_stack(cloudformation, user_type, stack_name, stack):
    """Verify access to create CloudFormation stacks.

    :param CloudFormation.Client cloudformation:
        Handle to the CloudFormation API.
    :param str user_type:
        The type of user running the test.
    :param str stack_name:
        Name of a CloudFormation stack.
    :param contextlib.ExitStack stack:
        An existing context manager that will help clean up after ourselves.

    """

    if user_type == "user":
        stack_name = "rhedcloudtestrhedcloudblockwritestatement"

    debug("Creating stack: {}".format(stack_name))
    res = cloudformation.create_stack(
        StackName=stack_name,
        TemplateBody=json.dumps({
            "AWSTemplateFormatVersion": "2010-09-09",
            "Description": "test, please delete",
            "Resources": {
                "S3Bucket": {
                    "Type": "AWS::S3::Bucket",
                    "Properties": {
                        "AccessControl": "Private",
                    },
                },
            },
        }),
    )
    assert has_status(res, 200), unexpected(res)

    stack.callback(
        ignore_errors(test_delete_stack),
        cloudformation,
        stack_name,
    )


@pytest.mark.skip("moved to scp: test_cloudformation.test_update_stack")
@pytest.blocked_user_only
def test_update_stack(cloudformation, stack_name):
    """Verify access to update CloudFormation stacks.

    :param CloudFormation.Client cloudformation:
        Handle to the CloudFormation API.
    :param str stack_name:
        Name of a CloudFormation stack.

    """

    debug("Updating stack: {}".format(stack_name))
    res = cloudformation.update_stack(
        StackName=stack_name,
        Tags=[{
            "Key": "test",
            "Value": "test",
        }],
    )
    assert has_status(res, 200), unexpected(res)


# @pytest.blocked_user_only
@pytest.admin_only
def test_delete_stack(cloudformation, stack_name):
    """Verify access to delete CloudFormation stacks.

    :param CloudFormation.Client cloudformation:
        Handle to the CloudFormation API.
    :param str stack_name:
        Name of a CloudFormation stack.

    """

    debug("Deleting stack: {}".format(stack_name))
    res = cloudformation.delete_stack(
        StackName=stack_name,
    )
    assert has_status(res, 200), unexpected(res)


##############################################################################
#                               LOGS TESTS                                   #
##############################################################################


group_names = pytest.mark.parametrize("group_name", (
    "rhedcloudtest",
    "test-FlowLogs",
    "/aws/lambda/rhedcloudtest",
))


@pytest.fixture
def logs(factory):
    return factory.client("logs")


@pytest.admin_only
@group_names
def test_create_log_group(logs, group_name, stack):
    """Verify access to create Logs groups.

    :param Logs.Client logs:
        Handle to the Logs API.
    :param str group_name:
        Name of a log group.
    :param contextlib.ExitStack stack:
        An existing context manager that will help clean up after ourselves.

    """

    debug("Creating log group: {}".format(group_name))
    res = logs.create_log_group(
        logGroupName=group_name,
    )
    assert has_status(res, 200), unexpected(res)

    debug("Created log group: {}".format(group_name))
    stack.callback(
        ignore_errors(test_delete_log_group),
        logs,
        group_name,
    )


@pytest.admin_only
@group_names
def test_create_log_streams(logs, group_name):
    """Verify access to create Logs streams.

    :param Logs.Client logs:
        Handle to the Logs API.
    :param str group_name:
        Name of a log group.

    """

    debug("Creating log stream: {}".format(group_name))
    res = logs.create_log_stream(
        logGroupName=group_name,
        logStreamName="test",
    )
    assert has_status(res, 200), unexpected(res)


@pytest.admin_only
@group_names
def test_put_log_events(logs, group_name):
    """Verify access to put events into Logs streams.

    :param Logs.Client logs:
        Handle to the Logs API.
    :param str group_name:
        Name of a log group.

    """

    debug("Putting log events: {}".format(group_name))
    res = logs.put_log_events(
        logGroupName=group_name,
        logStreamName="test",
        logEvents=[{
            "timestamp": 123,
            "message": "test",
        }],
    )
    assert has_status(res, 200), unexpected(res)


@pytest.mark.skip("moved to scp: test_logs.test_delete_log_group")
@pytest.blocked_user_only
@group_names
def test_delete_log_group(logs, group_name):
    """Verify access to delete Logs groups.

    :param Logs.Client logs:
        Handle to the Logs API.
    :param str group_name:
        Name of a log group.

    """

    debug("Deleting log group: {}".format(group_name))
    res = logs.delete_log_group(
        logGroupName=group_name,
    )
    assert has_status(res, 200), unexpected(res)


@pytest.mark.skip("moved to scp: test_logs.test_delete_metric_filter")
@pytest.blocked_user_only
@group_names
def test_delete_metric_filter(logs, group_name):
    """Verify access to delete Logs metric filters.

    :param Logs.Client logs:
        Handle to the Logs API.
    :param str group_name:
        Name of a log group.

    """

    debug("Deleting metric filter: {}".format(group_name))
    res = logs.delete_metric_filter(
        logGroupName=group_name,
        filterName="test",
    )
    assert has_status(res, 200), unexpected(res)


@pytest.mark.skip("moved to scp: test_logs.test_put_log_events")
@pytest.blocked_user_only
@group_names
def test_put_log_events_as_user(logs, group_name):
    test_put_log_events(logs, group_name)


@pytest.mark.skip("moved to scp: test_logs.test_create_log_group")
@pytest.blocked_user_only
@pytest.mark.parametrize("group_name", (
    "rhedcloudtest2",
    "test2-FlowLogs",
    "/aws/lambda/rhedcloudtest2",
))
def test_create_log_group_as_user(logs, group_name, stack):
    test_create_log_group(logs, group_name, stack)


##############################################################################
#                             EVENTS TESTS                                   #
##############################################################################


@pytest.fixture
def events(factory):
    return factory.client("events")


@pytest.fixture
def rule_name():
    return "rhedcloudtest_rhedcloud_block_write_statement"


@pytest.admin_only
def test_put_rule(events, rule_name, stack):
    """Verify access to create Events rules.

    :param Events.Client events:
        A handle to the Events API.
    :param str rule_name:
        Name of a rule.
    :param contextlib.ExitStack stack:
        An existing context manager that will help clean up after ourselves.

    """

    res = events.put_rule(
        Name=rule_name,
        ScheduleExpression="cron(0 20 * * ? *)",
        EventPattern=json.dumps({
            "source": ["aws.ec2"],
            "detail-type": ["EC2 Instance State-change Notification"],
            "detail": {
                "state": ["running"]
            }
        }),
        State="DISABLED",
        Description=rule_name,
    )
    assert has_status(res, 200), unexpected(res)

    debug("Created rule: {}".format(rule_name))
    stack.callback(
        ignore_errors(test_delete_rule),
        events,
        rule_name,
    )


@pytest.mark.skip("moved to scp: test_events.test_delete_rule")
@pytest.blocked_user_only
def test_delete_rule(events, rule_name):
    """Verify access to delete Events rules.

    :param Events.Client events:
        A handle to the Events API.
    :param str rule_name:
        Name of a rule.

    """

    debug("Deleting rule: {}".format(rule_name))
    res = events.delete_rule(
        Name=rule_name,
    )
    assert has_status(res, 200), unexpected(res)


@pytest.mark.skip("moved to scp: test_events.test_disable_rule")
@pytest.blocked_user_only
def test_disable_rule(events, rule_name):
    """Verify access to disable Events rules.

    :param Events.Client events:
        A handle to the Events API.
    :param str rule_name:
        Name of a rule.

    """

    debug("Disabling rule: {}".format(rule_name))
    res = events.disable_rule(
        Name=rule_name,
    )
    assert has_status(res, 200), unexpected(res)


@pytest.mark.skip("moved to scp: test_events.test_enable_rule")
@pytest.blocked_user_only
def test_enable_rule(events, rule_name):
    """Verify access to enable Events rules.

    :param Events.Client events:
        A handle to the Events API.
    :param str rule_name:
        Name of a rule.

    """

    debug("Enabling rule: {}".format(rule_name))
    res = events.enable_rule(
        Name=rule_name,
    )
    assert has_status(res, 200), unexpected(res)


@pytest.mark.skip("moved to scp: test_events.test_put_rule")
@pytest.blocked_user_only
def test_put_rule_as_user(events, rule_name, stack):
    test_put_rule(events, rule_name + "2", stack)
