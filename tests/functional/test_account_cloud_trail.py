'''
---------------------------
test_account_cloud_trail.py
---------------------------

"Type": "functional",
"Name": "test_account_cloud_trail",
"Description": "Create a logging event and confirm event is present in Cloud Trail logs.",
"Plan": "Create an event by stopping logging, then search cloud trail log for event.",
"ExpectedResult": "Success"

Create a logging event and confirm event is present in Cloud Trail logs.

Plans:

* create an event by stopping logging
* search cloud trail log for event

Expected Result: Success

${testcount:1}

'''

from datetime import datetime, timedelta
import time

import boto3
import pytest


@pytest.fixture(scope="module")
def cloudtrail():
    return boto3.client("cloudtrail")


@pytest.mark.skip("Retired: see commit 5c4c2d2 (9 Aug 2019)")
@pytest.mark.slowtest
def test_answer(cloudtrail):
    pom = timedelta(seconds=5)
    start = datetime.utcnow() - pom
    events_found = 0

    # find existing Cloud Trails
    trails = cloudtrail.describe_trails()
    assert len(trails['trailList']) != 0

    # use an existing Cloud Trail
    trail = trails['trailList'][0]
    print('Found trail:', trail['Name'])

    # disable and re-enable logging to create events in Cloud Trail
    try:
        cloudtrail.stop_logging(Name=trail['TrailARN'])
        time.sleep(1)
    finally:
        # always attempt to start logging again
        cloudtrail.start_logging(Name=trail['TrailARN'])

    end = datetime.utcnow() + pom

    # check for events every 30 seconds for up to 20 minutes
    timeout = start + timedelta(minutes=20)
    print("Looking up events between {} and {}".format(start, end))
    while datetime.now() < timeout:
        events = cloudtrail.lookup_events(StartTime=start, EndTime=end)
        events_found = 0
        for event in events['Events']:
            if event['EventName'] in ('StartLogging', 'StopLogging'):
                events_found += 1

        if events_found >= 2:
            break

        print('{} Events found: {}/{}'.format(
            datetime.utcnow().isoformat(),
            events_found,
            len(events['Events'])
        ))
        time.sleep(30)

    assert events_found == 2
