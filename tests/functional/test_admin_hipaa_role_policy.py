'''
============================
test_admin_hipaa_role_policy
============================

"Type": "functional",
"Name": "test_admin_hipaa_role_policy",
"Description": "Verify that the RHEDcloudAdministratorRoleHipaaPolicy policy is performing as expected against the specified resources.",
"Plan": "Attach RHEDcloudAdministratorRolePolicy and this policy, test each statement and expect AccessDenied errors for each blocked call.",
"ExpectedResult": "Failure"

This test verifies that the RHEDcloudAdministratorRoleHipaaPolicy policy is
disallowing users from performing the following actions on the
appropriate resources and allowing on other resources:

rds:CreateInstance


${testcount:3}

'''

import boto3
import pytest

from aws_test_functions import (
    has_status,
    ignore_errors,
    make_identifier,
    unexpected,
)

pytestmark = pytest.mark.skip("Now controlled by SCP rather than by IAM Policy")


@pytest.fixture(scope="module")
def admin_rds():
    return boto3.client("rds")


@pytest.fixture(scope="module")
def user_policies():
    return (
        "RHEDcloudAdministratorRolePolicy",
        "RHEDcloudAdministratorRoleHipaaPolicy",
        "AdministratorAccess",
    )


@pytest.fixture(scope="module")
def rds(session):
    yield session.client("rds")


@pytest.fixture(scope="module")
def identifier():
    return make_identifier("testhipaapolicy")


@pytest.fixture
def params(identifier):
    return dict(
        DBInstanceIdentifier=identifier,
        AllocatedStorage=20,
        DBInstanceClass='db.t2.micro',
        Engine='sqlserver-ex',
        MasterUsername='test',
        MasterUserPassword='testtest',
        BackupRetentionPeriod=0,
    )


@pytest.mark.raises_access_denied
def test_create_db_instance_non_compliant(rds, params, stack):
    """Verify access to create a non-compliant RDS instance.

    :param RDS.Client rds:
        Handle to the RDS API.
    :param dict params:
        Parameters to use when creating the instance.
    :param contextlib.ExitStack stack:
        An existing context manager that will help clean up after ourselves.

    """

    res = rds.create_db_instance(**params)
    assert has_status(res, 200), unexpected(res)

    stack.callback(
        ignore_errors(test_delete_db_instance),
        rds,
        params["DBInstanceIdentifier"],
    )


def test_create_db_instance_compliant(rds, params, stack):
    """Verify access to create a compliant RDS instance.

    :param RDS.Client rds:
        Handle to the RDS API.
    :param dict params:
        Parameters to use when creating the instance.
    :param contextlib.ExitStack sTack:
        An existing context manager that will help clean up after ourselves.

    """

    params.update(dict(
        AllocatedStorage=200,
        DBInstanceClass='db.m4.large',
        Engine='sqlserver-se',
        LicenseModel='license-included',
    ))

    res = rds.create_db_instance(**params)
    assert has_status(res, 200), unexpected(res)

    stack.callback(
        ignore_errors(test_delete_db_instance),
        rds,
        params["DBInstanceIdentifier"],
    )


def test_delete_db_instance(admin_rds, identifier):
    """Verify access to delete RDS instance.

    :param RDS.Client admin_rds:
        Handle to the RDS API.
    :param dict params:
        Parameters to use when creating the instance.

    """

    res = admin_rds.delete_db_instance(
        DBInstanceIdentifier=identifier,
        SkipFinalSnapshot=True
    )
    assert has_status(res, 200), unexpected(res)
