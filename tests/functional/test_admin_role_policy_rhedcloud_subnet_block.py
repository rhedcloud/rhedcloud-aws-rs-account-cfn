"""
=============================================
test_admin_role_policy_rhedcloud_subnet_block
=============================================

"Type": "functional",
"Name": "test_admin_role_policy_rhedcloud_subnet_block",
"Description": "Verify that the rhedcloudSubnetBlock statement within the RHEDcloudAdministratorRolePolicy policy is performing as expected against the specified resources.",
"Plan": "Attach *:* and this policy, test each statement and expect AccessDenied errors for each blocked call.",
"ExpectedResult": "Failure"

This test verifies that the rhedcloudSubnetBlock statement is
disallowing users from performing the following actions on the
appropriate resources and allowing on other resources:

ec2:TerminateInstances
ec2:StopInstances
ec2:StartInstances
ec2:CreateTags


${testcount:8}

"""

from botocore.exceptions import ClientError
import boto3
import pytest

from aws_test_functions import (
    aws_client,
    debug,
    has_status,
    unexpected,
    new_ec2_instance,
)


permitted = pytest.mark.raises(ClientError, match="would have succeeded")
pytestmark = [
    pytest.mark.parametrize("mode", (
        pytest.param("mgmt", marks=pytest.mark.raises_access_denied),
        pytest.param("customer", marks=permitted),
    )),
]


@aws_client("ec2")
def new_instance(mode, subnet_id, stack, *, ec2):
    """Create a new EC2 instance.

    :param str mode:
        Test mode (mgmt or customer).
    :param str subnet_id:
        ID of the subnet the EC2 instance should use.
    :param contextlib.ExitStack stack:
        An existing context manager that will help clean up after ourselves.

    :returns:
        An EC2.Instance resource.

    """

    tags = [{
        "Key": "Name",
        "Value": "test_rhedcloud_subnet_block_statement_{}".format(mode),
    }]

    if mode == "mgmt":
        tags.append({
            "Key": "RHEDcloud",
            "Value": "True",
        })

    params = dict(
        ami="ami-6057e21a",
        size="t2.micro",
        TagSpecifications=[{
            "ResourceType": "instance",
            "Tags": tags,
        }],
        Monitoring={
            "Enabled": False
        },
        NetworkInterfaces=[{
            "DeviceIndex": 0,
            "AssociatePublicIpAddress": False,
            "SubnetId": subnet_id,
        }],
    )

    return stack.enter_context(new_ec2_instance(mode, **params))


@pytest.fixture(scope="module")
def admin_ec2():
    return boto3.client("ec2")


@pytest.fixture(scope="module")
def ec2(session):
    return session.client("ec2")


@pytest.fixture(scope="module")
def subnet_id(admin_ec2):
    return admin_ec2.describe_subnets()["Subnets"][0]["SubnetId"]


@pytest.fixture(scope="module")
def mgmt_instance(admin_ec2, subnet_id, stack):
    yield new_instance("mgmt", subnet_id, stack, ec2=admin_ec2)


@pytest.fixture(scope="module")
def customer_instance(admin_ec2, subnet_id, stack):
    yield new_instance("customer", subnet_id, stack, ec2=admin_ec2)


@pytest.fixture
def instance_id(admin_ec2, mgmt_instance, customer_instance, mode):
    """Return the ID of the appropriate EC2 instance based on the test mode.

    :param EC2.Client admin_ec2:
        Handle to the EC2 API.
    :param EC2.Instance mgmt_instance:
        EC2 Instance resource representing a RHEDcloud instance.
    :param EC2.Instance customer_instance:
        EC2 Instance resource representing a customer instance.
    :param str mode:
        Test mode (mgmt or customer).

    :returns:
        A string.

    """

    instance = mgmt_instance if mode == "mgmt" else customer_instance

    debug("Waiting for instance: {}".format(instance.id))
    waiter = admin_ec2.get_waiter("instance_running")
    waiter.wait(
        InstanceIds=[instance.id],
        WaiterConfig={
            "Delay": 5,
            "MaxAttempts": 1000,
        }
    )

    return instance.id


def test_terminate_instance(ec2, instance_id):
    """Verify access to terminate EC2 instances.

    :param EC2.Client ec2:
        Handle to the EC2 API.
    :param str instance_id:
        An EC2 instance ID.

    """

    res = ec2.terminate_instances(
        InstanceIds=[instance_id],
        DryRun=True,
    )
    assert has_status(res, 200), unexpected(res)


def test_stop_instance(ec2, instance_id):
    """Verify access to stop EC2 instances.

    :param EC2.Client ec2:
        Handle to the EC2 API.
    :param str instance_id:
        An EC2 instance ID.

    """

    res = ec2.stop_instances(
        InstanceIds=[instance_id],
        DryRun=True,
    )
    assert has_status(res, 200), unexpected(res)


def test_start_instance(ec2, instance_id):
    """Verify access to start EC2 instances.

    :param EC2.Client ec2:
        Handle to the EC2 API.
    :param str instance_id:
        An EC2 instance ID.

    """

    res = ec2.start_instances(
        InstanceIds=[instance_id],
        DryRun=True,
    )
    assert has_status(res, 200), unexpected(res)


def test_create_tags(ec2, instance_id):
    """Verify access to tag EC2 instances.

    :param EC2.Client ec2:
        Handle to the EC2 API.
    :param str instance_id:
        An EC2 instance ID.

    """

    res = ec2.create_tags(
        Resources=[instance_id],
        DryRun=True,
        Tags=[{
            "Key": "RHEDcloud",
            "Value": "False",
        }],
    )
    assert has_status(res, 200), unexpected(res)
