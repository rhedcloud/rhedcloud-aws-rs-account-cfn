'''
-----------------
test_security_risk_detection_service_role.py
-----------------

Type": "functional",
Name": "test_security_risk_detection_service_role",
Description": "This test performs a series of positive and negative tests to verify the RHEDcloudSecurityRiskDetectionServiceRolePolicy.",
Plan": "Create a test user, attach the RHEDcloudSecurityRiskDetectionServiceRolePolicy, and perform a series of positive and negative tests as the test user.  (See test_central_admin_role.py for example tests - since the policy is currently the same)",
ExpectedResult": "Success"

This test performs a series of positive and negative tests to verify the
RHEDcloudSecurityRiskDetectionServiceRolePolicy.

Plans:

* create a test user
* attach the RHEDcloudSecurityRiskDetectionServiceRolePolicy
* perform positive tests as the test user
* perform negative tests as the test user

Note: Based heavily on test_central_admin_role.py since the policy is the same
as of initial writing.

${testcount:2}

'''

import boto3
import pytest

from aws_test_functions import (
    attach_policy,
    new_test_user,
)
from tests.functional.test_central_admin_role import (
    can_attach_deattach_special_roles_policy,
    can_change_bucket_policy_denied,
)

pytest_plugins = [
    'tests.functional.test_central_admin_role',
]


@pytest.fixture(scope='module')
def user():
    iam = boto3.resource('iam')

    with new_test_user('test_security_checker', iam=iam) as u:
        with attach_policy(u, 'rhedcloud/RHEDcloudSecurityRiskDetectionServiceRolePolicy'):
            yield u


def test_attach_roles(user):
    assert can_attach_deattach_special_roles_policy(user.user_name)


def test_change_bucket_policy(user, bucket):
    assert not can_change_bucket_policy_denied(bucket, user.user_name)
