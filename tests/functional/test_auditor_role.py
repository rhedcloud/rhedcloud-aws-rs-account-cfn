'''
-----------------
test_auditor_role.py
-----------------

"Type": "functional",
"Name": "test_auditor_role",
"Description": "This test performs a series of positive and negative tests to verify the auditor role's access.",
"Plan": "Create a test user, attach the ReadOnlyAccess AWS managed policy, and perform positive and negative tests to ensure the access is correct.",
"ExpectedResult": "Success"

This test verifies that the AWS Managed ReadOnlyAccess policy is
disallowing users from assuming the Auditor role and performing create,
delete, and update operations by running a set of positive and
negative tests.  The actions chosen for this test are meant to be
a sampling of representative tests on things that were provisioned
as part of the research service account cloudformation template.

Positive tests:
For these tests, create a user, attach the AWS Managed ReadOnlyAccess policy, and attempt the following actions:

P1. List users
P2. Get RHEDcloudAdministratorRolePolicy policy
P3. List roles
P4. List buckets
P5. Describe trails

This test creates a user that must be cleaned up before the test finishes, regardless of whether the test passes or fails.

Negative test:
N1. Attach policy to test user
N2. Detach policy from RHEDcloudAdministratorRole
N3. Create a role
N4. Delete bucket policy
N5. Stop cloudtrail logging -- RETIRED 8/9/2019 no longer creating CloudTrail in account

The above tests should all fail because these actions have not been explicitly allowed.

${testcount:10}

'''

import time

from botocore.exceptions import ClientError
import boto3
import json

from aws_test_functions import *

from tests.common import CLOUDTRAIL_BUCKET_SUFFIX


def get_cloudtrail_bucket_name(bucket_name_suffix):
    try:
        s3 = boto3.resource('s3')
        for bucket in s3.buckets.all():
            if bucket.name.endswith(bucket_name_suffix):
                return bucket.name
        return False
    except ClientError as err:
        print("Failed to create boto3 resource.\n" + str(err))
        return False


def test_answer():
    user = create_test_user('test_auditor_role')
    rhedcloudAuditorRolePolicyArn = "arn:aws:iam::aws:policy/ReadOnlyAccess"
    rhedcloudAdministratorRolePolicyArn = "arn:aws:iam::" + get_account_number() + ":policy/rhedcloud/RHEDcloudAdministratorRolePolicy"
    newAssumeRolePolicyUpdate = PolicyDocument='{"Version":"2012-10-17","Statement":[{"Effect":"Allow","Principal":{"Service":["ec2.amazonaws.com"]},"Action":["sts:AssumeRole"]}]}'
    originalAssumeRolePolicyUpdate = PolicyDocument = '{"Version":"2012-10-17","Statement":[{"Effect":"Allow","Principal":{"Service": "vpc-flow-logs.amazonaws.com"},"Action":["sts:AssumeRole"]}]}'
    attach_policy_to_user(user['UserName'], rhedcloudAuditorRolePolicyArn)
    session = create_session_for_test_user(user['UserName'])
    trail_client = session.client('cloudtrail')
    iam_client = session.client('iam')
    s3_client = session.client('s3')

    #Objects created for cleanup
    test_group ='test_group-'+get_uuid()
    test_user = 'test_user-'+get_uuid()
    timestamp_sec = int(time.time())
    timestamp_msec = timestamp_sec * 1000
    log_event = {'timestamp': timestamp_msec, 'message': 'Paul was here.'}

    position = None
    try:
        # Positive tests
        try:
            position = '#P1: List all users'
            iam_client.list_users()

            position = '#P2: Get RHEDcloudAdministratorRolePolicy policy'
            iam_client.get_policy(PolicyArn=rhedcloudAdministratorRolePolicyArn)

            position = '#P3: List roles'
            iam_client.list_roles()

            position = '#P4: List buckets'
            s3_client.list_buckets()

            position = '#P5: Describe trail'
            trail_client.describe_trails()

        except ClientError as e:
            print(position, ": FAILED")
            print("Positive test failed with error: %s" % e)
            assert False

        # Negative Tests
        try:
            position = '#N1: attach policy to test user'
            iam_client.attach_user_policy(UserName=test_user, PolicyArn=rhedcloudAdministratorRolePolicyArn)
            print(position, ": FAILED")
            print("Negative test failed with error: %s" % e)
            iam_client.detach_user_policy(UserName=test_user, PolicyArn=rhedcloudAdministratorRolePolicyArn)
            assert False

        except ClientError as e:
            if e.response['Error']['Code'] != 'AccessDenied':
                print(position, ": Unexpected error: %s" % e)
                assert False

        try:
            position = '#N2: detach RHEDcloudAdmnistratorRolePolicy from RHEDcloudAdministratorRole '
            iam_client.detach_role_policy(RoleName='RHEDcloudAdministratorRole', PolicyArn=rhedcloudAdministratorRolePolicyArn)
            print(position, ": FAILED")
            print("Positive test failed with error: %s" % e)
            iam_client.attach_role_policy(RoleName='RHEDcloudAdministratorRole', PolicyArn=rhedcloudAdministratorRolePolicyArn)
            assert False

        except ClientError as e:
            if e.response['Error']['Code'] != 'AccessDenied':
                print(position, ": Unexpected error: %s" % e)
                assert False

        try:
            position = '#N3: update assume role policy '
            iam_client.update_assume_role_policy(RoleName='RHEDcloudVpcFlowLogRole', PolicyDocument=newAssumeRolePolicyUpdate)
            print(position, ": FAILED")
            print("Positive test failed with error: %s" % e)
            iam_client.update_assume_role_policy(RoleName='RHEDcloudVpcFlowLogRole', PolicyDocument=originalAssumeRolePolicyUpdate)
            assert False

        except ClientError as e:
            if e.response['Error']['Code'] != 'AccessDenied':
                print(position, ": Unexpected error: %s" % e)
                assert False

        try:
            position = '#N4: delete bucket policy'
            ct_bucket = get_cloudtrail_bucket_name(CLOUDTRAIL_BUCKET_SUFFIX)
            s3_client.delete_bucket_policy(Bucket=ct_bucket)
            print(position, ": FAILED")
            print("Positive test failed with error: %s" % e)
            # Bucket Policy
            bucket_policy = {
                "Version": "2012-10-17",
                "Statement": [
                    {
                        "Sid": "AWSCloudTrailAclCheck",
                        "Effect": "Allow",
                        "Principal": {
                            "Service": "cloudtrail.amazonaws.com"
                        },
                        "Action": "s3:GetBucketAcl",
                        "Resource": resource1
                    },
                    {
                        "Sid": "AWSCloudTrailWrite",
                        "Effect": "Allow",
                        "Principal": {
                            "Service": "cloudtrail.amazonaws.com"
                        },
                        "Action": "s3:PutObject",
                        "Resource": resource2,
                        "Condition": {
                            "StringEquals": {
                                "s3:x-amz-acl": "bucket-owner-full-control"
                            }
                        }
                    }
                ]
            }

            # Convert policy to a JSON string
            bucket_policy = json.dumps(bucket_policy)
            s3_client.put_bucket_policy(Bucket=ct_bucket, Policy=bucket_policy)
            assert False

        except ClientError as e:
            if e.response['Error']['Code'] != 'AccessDenied':
                print(position, ": Unexpected error: %s" % e)
                assert False
                
        ''' RETIRING THIS TEST
        try:
            position = '#N5: stop cloudtrail logging'
            # determine trail name
            cloudtrail = boto3.client('cloudtrail')
            cdt = cloudtrail.describe_trails(trailNameList=[], includeShadowTrails=False)
            trail_name = cdt["trailList"][0]['Name']
            trail_client.stop_logging(Name=trail_name)
            print(position, ": FAILED")
            print("Positive test failed with error: %s" % e)
            trail_client.start_logging(Name=trail_name)
            assert False

        except ClientError as e:
            if e.response['Error']['Code'] != 'AccessDeniedException':
                print(position, ": Unexpected error: %s" % e)
                assert False
        '''
        assert True

    finally:
        # cleanup
        try:
            delete_test_user(user['UserName'])
        except ClientError as e:
            print(position, ":cleanup: Can't delete test user: %s" % e)
