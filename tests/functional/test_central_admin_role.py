'''
--------------------------
test_central_admin_role.py
--------------------------

"Type": "functional",
"Name": "test_central_admin_role",
"Description": "This test performs a series of positive and negative tests to verify the central administrator role inline policy.",
"Plan": "Create a test user, attach the same inline policy to the test user, and perform a series of negative and positive tests to ensure the inline policy is working as expected.",
"ExpectedResult": "Success"

This test verifies that the RHEDcloudCentralAdminstrator role's inline policy is
allowing the role to perform actions on the following roles
1."arn:aws:iam::*:role/RHEDcloudCentralAdministratorRole",
2."arn:aws:iam::*:role/RHEDcloudAdministratorRole"
3."arn:aws:iam::*:role/RHEDcloudSecurityRiskDetectionServiceRole"
4."arn:aws:iam::*:role/RHEDcloudAuditorRole"

denying the role to perform actions that are permitted for root user only

Negative tests::
N1. Change the policy of a s3 bucket that has deny all for the RHEDcloudCentralAdministrator
 User. Note that if a root user has been explicitly deny to a bucket, the user can change
 the bucket policy back to allow, but an IAM user would not able to do that.
 Ref: https://docs.aws.amazon.com/IAM/latest/UserGuide/troubleshoot_iam-s3.html

Positive tests:
P1. Attach a managed policy to a special role
P2. Detach the policy from a special role

This test creates users, roles and policies that must be cleaned up regardless of

${testcount:3}

'''

import json

from botocore.exceptions import ClientError
import pytest

from aws_test_functions import (
    attach_policy,
    create_session_for_test_user,
    create_test_policy_using_client,
    delete_test_policy_using_client,
    get_account_number,
    new_bucket,
    new_test_user,
)


def can_attach_deattach_special_roles_policy(userName):
    '''
        Test P1, P2
    '''

    session = create_session_for_test_user(userName)
    iamClient = session.client('iam')

    policyArn = create_test_policy_using_client(iamClient)['Policy']['Arn']
    roles = [
        'RHEDcloudCentralAdministratorRole',
        'RHEDcloudAdministratorRole',
        'RHEDcloudSecurityRiskDetectionServiceRole',
        'RHEDcloudAuditorRole',
        'RHEDcloudVpcFlowLogRole',
    ]

    try:

        for aRole in roles:
            iamClient.attach_role_policy(RoleName=aRole, PolicyArn=policyArn)
            iamClient.detach_role_policy(RoleName=aRole, PolicyArn=policyArn)
        return True

    except ClientError as err:
        print("Unexpected error when attaching or detaching policy \n" + format(err))

    finally:
        delete_test_policy_using_client(iamClient, policyArn)


def can_change_bucket_policy_denied(bucket, username):
    '''
        The test first creates a s3 bucket
        Then sets bucket policy to explicitly allow * by the central admin user
        Then sets bucket policy to explicitly deny * by the central admin user
        After changing to deny *, the central admin user should not be able to change policy back to allow *
        Although a root user should have been able to do so
    '''

    session = create_session_for_test_user(username)
    s3client = session.client('s3')
    ac_no = get_account_number()

    stmt = {
        "Version": "2012-10-17",
        "Statement": [{
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::{}:user/{}".format(ac_no, username),
            },
            "Action": "s3:*",
            "Resource": "arn:aws:s3:::{}".format(bucket.name),
        }],
    }

    allow_bucket_policy = json.dumps(stmt)

    stmt['Statement'][0]['Effect'] = 'Deny'
    deny_bucket_policy = json.dumps(stmt)

    print('About to set Allow * policy:', allow_bucket_policy)
    s3client.put_bucket_policy(Bucket=bucket.name, Policy=allow_bucket_policy)

    result = s3client.get_bucket_policy(Bucket=bucket.name)
    assert result

    print('About to set Deny * policy:', deny_bucket_policy)
    s3client.put_bucket_policy(Bucket=bucket.name, Policy=deny_bucket_policy)
    try:
        print('About to set Allow * policy again')
        s3client.put_bucket_policy(Bucket=bucket.name, Policy=allow_bucket_policy)
        return True
    except ClientError as err:
        if err.response['Error']['Code'] in ['AccessDenied']:
            return False
        else:
            print('Unexpected error when putting bucket policy \n' + format(err))
            raise


@pytest.fixture
def bucket():
    """
    Create new bucket using the default boto3 rather than test user so that
    the bucket clean-up will not be impacted by the policy changes made
    during this test.

    :yields:
        An S3.Bucket resource.

    """

    with new_bucket('test-central-admin-role') as b:
        yield b


def test_answer(bucket):
    with new_test_user('test_central_admin_role') as user:
        with attach_policy(user, 'arn:aws:iam::aws:policy/AdministratorAccess'):
            username = user.user_name
            assert can_attach_deattach_special_roles_policy(username)
            assert not can_change_bucket_policy_denied(bucket, username)
