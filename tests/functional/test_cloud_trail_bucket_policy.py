'''
---------------------------
test_cloud_trail_bucket_policy.py
---------------------------

"Type": "functional",
"Name": "test_cloud_trail_bucket_policy",
"Description": "Verify that the Cloud Trail bucket policy allows the basic s3 operations, such as GetBucketAcl and PutObject.",
"Plan": "Create a test user with a default deny everything policy, try to view acls, put an object and delete the policy for failures. Then try the similar operations by adding a test user with the permissions for success.",
"ExpectedResult": "Success"

This test verifies that the Cloud Trail bucket policy allows the following s3 operations on the Cloud Trail bucket:

    "s3:GetBucketAcl",
    "s3:PutObject"

This test creates a user with a default deny everything policy, then tries the following:

    N1: User tries to view the cloudtrail bucket acl
    N2: User tries to put object into the cloud trail bucket
    N3: User tries to delete the bucket policy

Then the test adds the test user as a principal to the bucket policy, and tries the following:

    P1: User tries to view the cloudtrail bucket acl
    P2: User tries to put object into the cloud trail bucket
    N3: User tries to delete the bucket policy

The test then puts the bucket policy back and deletes the test user.

    ${testcount:6}

'''

import boto3
import time
import os
import json
from aws_test_functions import *
from botocore.exceptions import ClientError
from tests.common import CLOUDTRAIL_BUCKET_SUFFIX


def get_cloudtrail_bucket_name(bucket_name_suffix):
    try:
        s3 = boto3.resource('s3')
        for bucket in s3.buckets.all():
            if bucket.name.endswith(bucket_name_suffix):
                return bucket.name
        return False
    except ClientError as err:
        print("Failed to create boto3 resource.\n" + str(err))
        return False


def test_answer():

    admin_s3_client = boto3.client('s3')

    ct_bucket = get_cloudtrail_bucket_name(CLOUDTRAIL_BUCKET_SUFFIX)
    ct_bucket_key = 'AWSLogs/' + get_account_number() + '/testing.txt'
    ct_bucket_policy = admin_s3_client.get_bucket_policy(Bucket=ct_bucket)
    original_bucket_policy = ct_bucket_policy['Policy']

    user1 = create_test_user('testuser_without_permissions')
    user1_arn = user1['Arn']
    session1 = create_session_for_test_user(user1['UserName'])
    user_s3_client1 = session1.client('s3')

    position = None
    try:

        try:
            position = '#N1: get the cloudtrail bucket acl'
            user_s3_client1.get_bucket_acl(Bucket=ct_bucket)
            print (position,": FAILED")
            print ("Test user is able to get bucket acl.")
            assert False

        except ClientError as e:
            if e.response['Error']['Code'] != 'AccessDenied':
               print (position,": Unexpected error: %s" % e)
               assert False

        try:
            position = '#N2: put object into the cloud trail bucket'
            user_s3_client1.put_object(
                ACL='bucket-owner-full-control',
                Body='testing',
                Bucket=ct_bucket,
                Key=ct_bucket_key
            )
            print (position,": FAILED")
            print ("Test user is able to put object into the cloud trail bucket")
            assert False

        except ClientError as e:
            if e.response['Error']['Code'] != 'AccessDenied':
               print (position,": Unexpected error: %s" % e)
               assert False

        try:
            position = '#N3: delete the bucket policy'
            user_s3_client1.delete_bucket_policy(Bucket=ct_bucket)
            print (position,": FAILED")
            print ("Positive test failed with error: %s" % e)
            assert False

        except ClientError as e:
            if e.response['Error']['Code'] != 'AccessDenied':
               print (position,": Unexpected error: %s" % e)
               assert False

        #Attach test_policy to bucket and create new user session
        user2 = create_test_user('testuser_with_permissions')
        user2_arn = user2['Arn']

        #Insert delay to insure user is created before trying to add user to the bucket policy.
        time.sleep(10)

        test_bucket_policy = original_bucket_policy.replace('{"Service":"cloudtrail.amazonaws.com"}','{"AWS":"' + user2_arn +'"}')
        admin_s3_client.put_bucket_policy(Bucket=ct_bucket,Policy=test_bucket_policy)
        session2 = create_session_for_test_user(user2['UserName'])
        user_s3_client2 = session2.client('s3')

        try:
            position = '#P1: view the cloudtrail bucket acl'
            user_s3_client2.get_bucket_acl(Bucket=ct_bucket)

            position = '#P2: put object into the cloud trail bucket'
            user_s3_client2.put_object(
                ACL='bucket-owner-full-control',
                Body='testing',
                Bucket=ct_bucket,
                Key=ct_bucket_key
            )

        except ClientError as e:
            print (position,": FAILED")
            print ("Positive test failed with error: %s" % e)
            assert False

        try:
            position = '#N4: delete the bucket policy'
            user_s3_client2.delete_bucket_policy(Bucket=ct_bucket)
            print (position,": FAILED")
            print ("Negative test failed with error: %s" % e)
            assert False

        except ClientError as e:
            if e.response['Error']['Code'] != 'AccessDenied':
               print (position,": Unexpected error: %s" % e)
               assert False

        assert True

    finally:
        #cleanup
        delete_test_user(user1['UserName'])
        delete_test_user(user2['UserName'])
        admin_s3_client.put_bucket_policy(Bucket=ct_bucket,Policy=original_bucket_policy)
        admin_s3_client.delete_object(Bucket=ct_bucket,Key=ct_bucket_key)
        print('The end.')
