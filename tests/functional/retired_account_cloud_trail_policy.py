import boto3
from aws_test_functions import *
from botocore.exceptions import ClientError
from datetime import datetime
from time import sleep
import json
from random import randint
import os

import pprint

'''
    This test verifies that the RHEDcloudAccountCloudTrailPolicy is
    disallowing users from performing the following actions on the
    appropriate resources:

    "cloudtrail:CreateTrail",
    "cloudtrail:AddTags",
    "cloudtrail:UpdateTrail"
    "cloudtrail:DeleteTrail",
    "cloudtrail:GetEventSelectors",
    "cloudtrail:GetTrailStatus",
    "cloudtrail:PutEventSelectors",
    "cloudtrail:RemoveTags",
    "cloudtrail:StartLogging",
    "cloudtrail:StopLogging",
    "s3:DeleteBucket",
    "s3:DeleteBucketPolicy",
    "s3:DeleteBucketWebsite",
    "s3:PutBucketAcl",
    "s3:PutBucketCORS",
    "s3:PutBucketLogging",
    "s3:PutBucketNotification",
    "s3:PutBucketPolicy",
    "s3:PutBucketRequestPayment",
    "s3:PutBucketTagging",
    "s3:PutBucketVersioning",
    "s3:PutBucketWebsite",
    "s3:PutInventoryConfiguration",
    "s3:PutLifecycleConfiguration",
    "s3:PutMetricsConfiguration",
    "s3:PutReplicationConfiguration"
    "s3:DeleteObject",
    "s3:DeleteObjectTagging",
    "s3:DeleteObjectVersion",
    "s3:DeleteObjectVersionTagging",
    "s3:PutObject",
    "s3:PutObjectTagging",
    "s3:PutObjectVersionAcl",
    "s3:PutObjectVersionTagging",
    "s3:RestoreObject",
    "s3:PutObjectAcl"

    ${testcount:36}
'''

def failed_test(resource, reason):
    print(resource, "\n", reason, "\n")
    assert False

def passed_test(resource, reason):
    print(resource, "\n", reason, "\n")
    assert True

def admin_delete_bucket(s3_bucket_name):
    empty_bucket(s3_bucket_name)
    sleep(2)
    delete_bucket(s3_bucket_name)
    print("Deleted S3 Bucket: " + s3_bucket_name)

def test_answer():
    # s3_bucket_name = "serviceforge-aws-admin-105-ct1"
    # trail_name = 'rhedcloud-aws-rs-account-Master'
    s3_bucket_name = os.environ['CLOUDTRAIL_BUCKET_NAME']
    trail_name = os.environ['CLOUDTRAIL_NAME']
    user = create_test_user('test_account_cloud_trail_policy')
    rhedcloudAdminRolePolicyArn = "arn:aws:iam::"+get_account_number()+":policy/RHEDcloudAdministratorRolePolicy"
    attach_policy_to_user(user['UserName'],rhedcloudAdminRolePolicyArn)
    session = create_session_for_test_user(user['UserName'])
    admin_iam = boto3.client('iam')
    user_cloudtrail = session.client('cloudtrail')
    admin_cloudtrail = boto3.client('cloudtrail')
    user_s3 = session.client('s3')
    admin_s3 = boto3.client('s3')
    response = admin_s3.list_buckets()
    tmp_admin_display_name = response['Owner']['DisplayName']
    tmp_admin_id = response['Owner']['ID']
    logging_bucket_policy = admin_s3.get_bucket_policy(Bucket=s3_bucket_name)

    trail_name_change = None
    bucket_acl_change = None
    bucket_website_change = None
    bucket_inventory_change = None
    bucket_lifecyc_change = None
    bucket_metric_change = None
    bucket_object_change = None
    bucket_policy_change = None
    trail_change = None
    trail_tag_change = None
    trail_log_change = None
    bucket_cors_change = None

    try:
        # s3:PutBucketAcl
        try:
            response = user_s3.put_bucket_acl(
                AccessControlPolicy={
                    'Grants': [
                        {
                            'Grantee': {
                                'DisplayName': tmp_admin_display_name,
                                'ID': tmp_admin_id,
                                'Type': 'CanonicalUser'
                            },
                            'Permission': 'FULL_CONTROL'
                        }
                    ],
                    'Owner': {
                        'DisplayName': tmp_admin_display_name,
                        'ID': tmp_admin_id
                    }
                },
                Bucket=s3_bucket_name
            )
            bucket_acl_change = 'yes'

            failed_test('Permissions', 's3:PutBucketAcl')
        except ClientError as e:
            if e.response['Error']['Code'] == 'AccessDenied':
                print("Expected error, test successful.")
            else:
                failed_test('Error', 's3:PutBucketAcl: ' + str(e))

        # s3:PutBucketCORS
        try:
            response = user_s3.put_bucket_cors(
                Bucket=s3_bucket_name,
                CORSConfiguration={
                    'CORSRules': [
                        {
                            'AllowedHeaders': [
                                'Authorization',
                            ],
                            'AllowedMethods': [
                                'GET',
                            ],
                            'AllowedOrigins': [
                                '*',
                            ],
                            'MaxAgeSeconds': 12
                        }
                    ]
                }
            )
            bucket_cors_change = "yes"

            failed_test('Permissions', 's3:PutBucketCORS')
        except ClientError as e:
            if e.response['Error']['Code'] == 'AccessDenied':
                print("Expected error, test successful.")
            else:
                failed_test('Error', 's3:PutBucketCORS: ' + str(e))

        # s3:PutBucketLogging
        try:
            response = user_s3.put_bucket_logging(
                Bucket=s3_bucket_name,
                BucketLoggingStatus={}
            )

            failed_test('Permissions', 's3:PutBucketLogging')
        except ClientError as e:
            if e.response['Error']['Code'] == 'AccessDenied':
                print("Expected error, test successful.")
            else:
                print("Unexpected error")
                failed_test('Error', 's3:PutBucketLogging: ' + str(e))

        # s3:PutBucketNotification
        try:
            response = user_s3.put_bucket_notification_configuration(
                Bucket=s3_bucket_name,
                NotificationConfiguration={}
            )

            failed_test('Permissions', 's3:PutBucketNotification')
        except ClientError as e:
            if e.response['Error']['Code'] == 'AccessDenied':
                print("Expected error, test successful.")
            else:
                failed_test('Error', 's3:PutBucketNotification: ' + str(e))

        # s3:PutBucketPolicy
        try:
            response = admin_s3.get_bucket_policy(
                Bucket=s3_bucket_name
            )
            response = user_s3.put_bucket_policy(
                Bucket=s3_bucket_name,
                Policy=response['Policy']
            )

            failed_test('Permissions', 's3:PutBucketPolicy')
        except ClientError as e:
            if e.response['Error']['Code'] == 'AccessDenied':
                print("Expected error, test successful.")
            else:
                failed_test('Error', 's3:PutBucketPolicy: ' + str(e))

        # s3:PutBucketRequestPayment
        try:
            response = user_s3.put_bucket_request_payment(
                Bucket=s3_bucket_name,
                RequestPaymentConfiguration={
                    'Payer': 'BucketOwner'
                }
            )

            failed_test('Permissions', 's3:PutBucketRequestPayment')
        except ClientError as e:
            if e.response['Error']['Code'] == 'AccessDenied':
                print("Expected error, test successful.")
            else:
                failed_test('Error', 's3:PutBucketRequestPayment: ' + str(e))

        # s3:PutBucketTagging
        try:
            response = user_s3.put_bucket_tagging(
                Bucket=s3_bucket_name,
                Tagging={
                    'TagSet': [
                        {
                            'Key': 'string',
                            'Value': 'string'
                        },
                    ]
                }
            )

            failed_test('Catastrophic Error Permissions', 's3:PutBucketTagging')
        except ClientError as e:
            if e.response['Error']['Code'] == 'AccessDenied':
                print("Expected error, test successful.")
            else:
                failed_test('Error', 's3:PutBucketTagging: ' + str(e))

        # s3:PutBucketVersioning
        try:
            response = user_s3.put_bucket_versioning(
                Bucket=s3_bucket_name,
                VersioningConfiguration={
                    'Status': 'Suspended'
                }
            )

            failed_test('Permissions', 's3:PutBucketVersioning')
        except ClientError as e:
            if e.response['Error']['Code'] == 'AccessDenied':
                print("Expected error, test successful.")
            else:
                failed_test('Error', 's3:PutBucketVersioning: ' + str(e))

        # s3:PutBucketWebsite
        try:
            response = user_s3.put_bucket_website(
                Bucket=s3_bucket_name,
                WebsiteConfiguration={
                    "IndexDocument": {
                        "Suffix": "index.html"
                    }
                }
            )
            bucket_website_change = "yes"

            failed_test('Permissions', 's3:PutBucketWebsite')
        except ClientError as e:
            if e.response['Error']['Code'] == 'AccessDenied':
                print("Expected error, test successful.")
            else:
                failed_test('Error', 's3:PutBucketWebsite: ' + str(e))

        # s3:PutInventoryConfiguration
        try:
            response = user_s3.put_bucket_inventory_configuration(
                Bucket=s3_bucket_name,
                Id="test",
                InventoryConfiguration={
                    "Schedule": {
                        "Frequency": "Weekly"
                    },
                    "IsEnabled": False,
                    "Destination": {
                        "S3BucketDestination": {
                            "Bucket": "arn:aws:s3:::" + s3_bucket_name,
                            "Format": "CSV"
                        }
                    },
                    "IncludedObjectVersions": "Current",
                    "Id": "test"
                }
            )
            bucket_inventory_change = "yes"

            failed_test('Permissions', 's3:PutInventoryConfiguration')
        except ClientError as e:
            if e.response['Error']['Code'] == 'AccessDenied':
                print("Expected error, test successful.")
            else:
                failed_test('Error', 's3:PutInventoryConfiguration: ' + str(e))

        # s3:PutLifecycleConfiguration
        try:
            response = user_s3.put_bucket_lifecycle_configuration(
                Bucket=s3_bucket_name,
                LifecycleConfiguration={
                    "Rules": [
                        {
                            "Filter": {
                                "Prefix": "test"
                            },
                            "Status": "Disabled",
                            "Expiration": {
                                "ExpiredObjectDeleteMarker": False
                            },
                            "ID": "test"
                        }
                    ]
                }
            )
            bucket_lifecyc_change = "yes"

            failed_test('Permissions', 's3:PutLifecycleConfiguration')
        except ClientError as e:
            if e.response['Error']['Code'] == 'AccessDenied':
                print("Expected error, test successful.")
            else:
                failed_test('Error', 's3:PutLifecycleConfiguration: ' + str(e))

        # s3:PutMetricsConfiguration
        try:
            response = user_s3.put_bucket_metrics_configuration(
                Bucket=s3_bucket_name,
                Id="test",
                MetricsConfiguration={
                    "Filter": {
                        "Prefix": "test"
                    },
                    "Id": "test"
                }
            )
            bucket_metric_change = "yes"

            failed_test('Permissions', 's3:PutMetricsConfiguration')
        except ClientError as e:
            if e.response['Error']['Code'] == 'AccessDenied':
                print("Expected error, test successful.")
            else:
                failed_test('Error', 's3:PutMetricsConfiguration: ' + str(e))

        # s3:PutReplicationConfiguration
        try:
            response = user_s3.put_bucket_replication(
                Bucket=s3_bucket_name,
                ReplicationConfiguration={
                    'Role': "arn:aws:iam::"+get_account_number()+":role/test",
                    'Rules': [
                        {
                            'ID': 'mytestid',
                            'Prefix': '',
                            'Status': 'Disabled',
                            'Destination': {
                                'Bucket': 'arn:aws:s3:::' + s3_bucket_name,
                                'StorageClass': 'STANDARD'
                            }
                        },
                    ]
                }
            )

            failed_test('Permissions', 's3:PutReplicationConfiguration')
        except ClientError as e:
            if e.response['Error']['Code'] == 'AccessDenied':
                print("Expected error, test successful.")
            elif e.response['Error']['Code'] == 'InvalidRequest':
                print("Expected error: InvalidRequest, test successful.")
            else:
                failed_test('Error', 's3:PutReplicationConfiguration: ' + str(e))

        # s3:PutObject
        try:
            response = user_s3.put_object(
                ACL='authenticated-read',
                Body=b'testing',
                Bucket=s3_bucket_name,
                Key='test.txt'
            )
            bucket_object_change = "yes"

            failed_test('Permissions', 's3:PutObject')
        except ClientError as e:
            if e.response['Error']['Code'] == 'AccessDenied':
                print("Expected error, test successful.")
                response = admin_s3.put_object(
                    ACL='authenticated-read',
                    Body=b'testing',
                    Bucket=s3_bucket_name,
                    Key='test.txt'
                )
                bucket_object_change = "yes"
            else:
                failed_test('Error', 's3:PutObject: ' + str(e))

        # s3:PutObjectTagging
        try:
            response = user_s3.put_object_tagging(
                Bucket=s3_bucket_name,
                Key='test.txt',
                Tagging={
                    'TagSet': [
                        {
                            'Key': 'string',
                            'Value': 'string'
                        },
                    ]
                }
            )

            failed_test('Permissions', 's3:PutObjectTagging')
        except ClientError as e:
            if e.response['Error']['Code'] == 'AccessDenied':
                print("Expected error, test successful.")
                try:
                    response = admin_s3.put_object_tagging(
                        Bucket=s3_bucket_name,
                        Key='test.txt',
                        Tagging={
                            'TagSet': [
                                {
                                    'Key': 'string',
                                    'Value': 'string'
                                },
                            ]
                        }
                    )
                except ClientError as e:
                    if e.response['Error']['Code'] == 'AccessDenied':
                        failed_test('Error', 's3:PutObjectTagging: Boto user does not have permissions to perform this action')
                    else:
                        failed_test('Error', 's3:PutObjectTagging: ' + str(e))
            else:
                failed_test('Error', 's3:PutObjectTagging: ' + str(e))

        # s3:PutObjectVersionTagging
        try:
            response = user_s3.put_object_tagging(
                Bucket=s3_bucket_name,
                Key='test.txt',
                VersionId='randomversionid',
                Tagging={
                    'TagSet': [
                        {
                            'Key': 'string',
                            'Value': 'string'
                        },
                    ]
                }
            )

            failed_test('Permissions', 's3:PutObjectVersionTagging')
        except ClientError as e:
            if e.response['Error']['Code'] == 'InvalidArgument':
                print("Expected error, test successful.")
            else:
                failed_test('Error', 's3:PutObjectVersionTagging: ' + str(e))

        # s3:PutObjectAcl
        try:
            response = admin_s3.get_object_acl(
                Bucket=s3_bucket_name,
                Key='test.txt'
            )
            # Remove extra element in policy
            tmp = None
            for element in response:
                if element == "ResponseMetadata":
                    tmp = "yes"
            if tmp != None:
                response.pop("ResponseMetadata")
            response = user_s3.put_object_acl(
                AccessControlPolicy=response,
                Bucket=s3_bucket_name,
                Key='test.txt'
            )

            failed_test('Permissions', 's3:PutObjectAcl')
        except ClientError as e:
            if e.response['Error']['Code'] == 'AccessDenied':
                print("Expected error, test successful.")
            else:
                failed_test('Error', 's3:PutObjectAcl: ' + str(e))

        # s3:PutObjectVersionAcl
        try:
            response = admin_s3.get_object_acl(
                Bucket=s3_bucket_name,
                Key='test.txt',
                VersionId='randomversionid'
            )
            # Remove extra element in policy
            tmp = None
            for element in response:
                if element == "ResponseMetadata":
                    tmp = "yes"
            if tmp != None:
                response.pop("ResponseMetadata")
            response = user_s3.put_object_acl(
                AccessControlPolicy=response,
                Bucket=s3_bucket_name,
                Key='test.txt',
                VersionId='randomversionid'
            )

            failed_test('Permissions', 's3:PutObjectVersionAcl')
        except ClientError as e:
            if e.response['Error']['Code'] == 'InvalidArgument':
                print("Expected error, test successful.")
            else:
                failed_test('Error', 's3:PutObjectVersionAcl: ' + str(e))

        # s3:DeleteObjectTagging
        try:
            response = user_s3.delete_object_tagging(
                Bucket=s3_bucket_name,
                Key='test.txt'
            )

            failed_test('Permissions', 's3:DeleteObjectTagging')
        except ClientError as e:
            if e.response['Error']['Code'] == 'AccessDenied':
                print("Expected error, test successful.")
            else:
                failed_test('Error', 's3:DeleteObjectTagging: ' + str(e))

        # s3:DeleteObjectVersionTagging
        try:
            response = user_s3.delete_object_tagging(
                Bucket=s3_bucket_name,
                Key='test.txt',
                VersionId='randomversionid'
            )

            failed_test('Permissions', 's3:DeleteObjectVersionTagging')
        except ClientError as e:
            if e.response['Error']['Code'] == 'InvalidArgument':
                print("Expected error, test successful.")
            else:
                failed_test('Error', 's3:DeleteObjectVersionTagging: ' + str(e))

        # s3:DeleteObject
        try:
            response = user_s3.delete_object(
                Bucket=s3_bucket_name,
                Key='test.txt'
            )
            bucket_object_change = None

            failed_test('Permissions', 's3:DeleteObject')
        except ClientError as e:
            if e.response['Error']['Code'] == 'AccessDenied':
                print("Expected error, test successful.")
            else:
                failed_test('Error', 's3:DeleteObject: ' + str(e))

        # s3:DeleteObjectVersion
        try:
            response = user_s3.delete_object(
                Bucket=s3_bucket_name,
                Key='test.txt',
                VersionId='randomversionid'
            )
            bucket_object_change = None

            failed_test('Permissions', 's3:DeleteObjectVersion')
        except ClientError as e:
            if e.response['Error']['Code'] == 'InvalidArgument':
                print("Expected error, test successful.")
            else:
                failed_test('Error', 's3:DeleteObjectVersion: ' + str(e))

        # s3:DeleteBucketWebsite
        try:
            response = user_s3.delete_bucket_website(
                Bucket=s3_bucket_name
            )
            bucket_website_change = None

            failed_test('Permissions', 's3:DeleteBucketWebsite')
        except ClientError as e:
            if e.response['Error']['Code'] == 'AccessDenied':
                print("Expected error, test successful.")
            else:
                failed_test('Error', 's3:DeleteBucketWebsite: ' + str(e))

        # s3:DeleteBucketPolicy
        try:
            response = user_s3.delete_bucket_policy(
                Bucket=s3_bucket_name
            )
            bucket_policy_change = "yes"

            failed_test('Permissions', 's3:DeleteBucketPolicy')
        except ClientError as e:
            if e.response['Error']['Code'] == 'AccessDenied':
                print("Expected error, test successful.")
            else:
                failed_test('Error', 's3:DeleteBucketPolicy: ' + str(e))

        # s3:DeleteBucket
        try:
            response = user_s3.delete_bucket(
                Bucket=s3_bucket_name
            )

            failed_test('Catastrophic Error: Permissions', 's3:DeleteBucket')
        except ClientError as e:
            if e.response['Error']['Code'] == 'AccessDenied':
                print("Expected error, test successful.")
            else:
                failed_test('Error', 's3:DeleteBucket: ' + str(e))

        # cloudtrail:CreateTrail
        try:
            response = user_cloudtrail.create_trail(
                Name=trail_name,
                S3BucketName=s3_bucket_name,
                IncludeGlobalServiceEvents=False,
                IsMultiRegionTrail=False
            )
            trail_name_change = 'yes'

            failed_test('Permissions', 'cloudtrail:CreateTrail')
        except ClientError as e:
            if e.response['Error']['Code'] == 'AccessDeniedException':
                print("Expected error, test successful.")
                try:
                    trail = admin_cloudtrail.describe_trails(
                        trailNameList=[
                            trail_name
                        ]
                    )
                    trail_arn = trail['trailList'][0]['TrailARN']
                except ClientError as e:
                    if e.response['Error']['Code'] == 'AccessDeniedException':
                        failed_test('Error', 'cloudtrail:DescribeTrail: Boto user does not have permissions to perform this action')
                    else:
                        failed_test('Error', 'cloudtrail:DescribeTrail: ' + str(e))
            elif e.response['Error']['Code'] == 'TrailAlreadyExistsException':
                print("Trail already exists.")
                failed_test('Error', 'cloudtrail:CreateTrail: ' + str(e))
            else:
                print("Unexpected error")
                failed_test('Error', 'cloudtrail:CreateTrail: ' + str(e))

        # cloudtrail:AddTags
        try:
            response = user_cloudtrail.add_tags(
                ResourceId=trail_arn,
                TagsList=[
                    {
                        'Key': 'string',
                        'Value': 'string'
                    },
                ]
            )
            trail_tag_change = "yes"

            failed_test('Permissions', 'cloudtrail:AddTags')
        except ClientError as e:
            if e.response['Error']['Code'] == 'AccessDeniedException':
                print("Expected error, test successful.")
            else:
                failed_test('Error', 'cloudtrail:AddTags: ' + str(e))

        # cloudtrail:UpdateTrail
        try:
            response = user_cloudtrail.update_trail(
                Name=trail_name,
                IsMultiRegionTrail=True
            )

            failed_test('Permissions', 'cloudtrail:UpdateTrail')
        except ClientError as e:
            if e.response['Error']['Code'] == 'AccessDeniedException':
                print("Expected error, test successful.")
            else:
                failed_test('Error', 'cloudtrail:UpdateTrail: ' + str(e))

        # cloudtrail:DeleteTrail
        try:
            response = user_cloudtrail.delete_trail(
                Name=trail_name
            )

            failed_test('Catastrophic Error: Permissions', 'cloudtrail:DeleteTrail')
        except ClientError as e:
            if e.response['Error']['Code'] == 'AccessDeniedException':
                print("Expected error, test successful.")
            else:
                failed_test('Error', 'cloudtrail:DeleteTrail: ' + str(e))

        # cloudtrail:GetEventSelectors
        try:
            response = user_cloudtrail.get_event_selectors(
                TrailName=trail_name
            )

        except ClientError as e:
            if e.response['Error']['Code'] == 'AccessDeniedException':
                failed_test('Error', 'cloudtrail:GetEventSelectors: ' + str(e))
                
        # cloudtrail:GetTrailStatus
        try:
            response = user_cloudtrail.get_trail_status(
                Name=trail_name
            )

        except ClientError as e:
            if e.response['Error']['Code'] == 'AccessDeniedException':
                failed_test('Error', 'cloudtrail:GetTrailStatus: ' + str(e))                

        # cloudtrail:PutEventSelectors
        try:
            response = admin_cloudtrail.get_event_selectors(
                TrailName=trail_name
            )
            response = user_cloudtrail.put_event_selectors(
                TrailName=trail_name,
                EventSelectors=response['EventSelectors']
            )

            failed_test('Permissions', 'cloudtrail:PutEventSelectors')
        except ClientError as e:
            if e.response['Error']['Code'] == 'AccessDeniedException':
                print("Expected error, test successful.")
            else:
                failed_test('Error', 'cloudtrail:PutEventSelectors: ' + str(e))

        # cloudtrail:RemoveTags
        try:
            response = admin_cloudtrail.add_tags(
                ResourceId=trail_arn,
                TagsList=[
                    {
                        'Key': 'string',
                        'Value': 'string'
                    },
                ]
            )
            trail_tag_change = "yes"
            response = user_cloudtrail.remove_tags(
                ResourceId=trail_arn,
                TagsList=[
                    {
                        'Key': 'string',
                        'Value': 'string'
                    },
                ]
            )
            trail_tag_change = None

            failed_test('Permissions', 'cloudtrail:RemoveTags')
        except ClientError as e:
            if e.response['Error']['Code'] == 'AccessDeniedException':
                print("Expected error, test successful.")
            else:
                failed_test('Error', 'cloudtrail:RemoveTags: ' + str(e))

        # cloudtrail:StartLogging
        try:
            response = user_cloudtrail.start_logging(
                Name=trail_name
            )

            failed_test('Permissions', 'cloudtrail:StartLogging')
        except ClientError as e:
            if e.response['Error']['Code'] == 'AccessDeniedException':
                print("Expected error, test successful.")
            else:
                failed_test('Error', 'cloudtrail:StartLogging: ' + str(e))

        # cloudtrail:StopLogging
        try:
            response = user_cloudtrail.stop_logging(
                Name=trail_name
            )
            trail_log_change = "yes"

            failed_test('Permissions', 'cloudtrail:StopLogging')
        except ClientError as e:
            if e.response['Error']['Code'] == 'AccessDeniedException':
                print("Expected error, test successful.")
            else:
                failed_test('Error', 'cloudtrail:StopLogging: ' + str(e))

        # s3:RestoreObject
        try:
            response = user_s3.restore_object(
                Bucket=s3_bucket_name,
                Key='thisismyrandomkey'
            )

            failed_test('Permissions', 's3:RestoreObject')
        except ClientError as e:
            if e.response['Error']['Code'] == 'AccessDenied':
                print("Expected error, test successful.")
            elif e.response['Error']['Code'] == 'InvalidObjectState':
                print("Object isnt available in Glacier.")
            else:
                failed_test('Error', 's3:RestoreObject: ' + str(e))

        passed_test("Passed", "Passed")
    finally:
        print("Finally Clean Up")
        print(user, s3_bucket_name, "\n", admin_s3, "\n", admin_iam, "\n", trail_name)
        if s3_bucket_name:
            if bucket_acl_change != None:
                response = admin_s3.put_bucket_acl(
                    AccessControlPolicy={
                        'Grants': [
                            {
                                'Grantee': {
                                    'DisplayName': tmp_admin_display_name,
                                    'ID': tmp_admin_id,
                                    'Type': 'CanonicalUser'
                                },
                                'Permission': 'FULL_CONTROL'
                            }
                        ],
                        'Owner': {
                            'DisplayName': tmp_admin_display_name,
                            'ID': tmp_admin_id
                        }
                    },
                    Bucket=s3_bucket_name
                )
                print("Fixed bucket ACL")

            if bucket_cors_change != None:
                response = admin_s3.delete_bucket_cors(
                    Bucket=s3_bucket_name
                )
                print("Removed bucket CORS")

            if bucket_website_change != None:
                response = admin_s3.delete_bucket_website(
                    Bucket=s3_bucket_name
                )
                print("Removed bucket website")

            if bucket_inventory_change != None:
                response = admin_s3.delete_bucket_inventory_configuration(
                    Bucket=s3_bucket_name,
                    Id='test'
                )
                print("Removed bucket inventory")

            if bucket_lifecyc_change != None:
                response = admin_s3.delete_bucket_lifecycle(
                    Bucket=s3_bucket_name
                )
                print("Removed bucket lifecycle")

            if bucket_metric_change != None:
                response = admin_s3.delete_bucket_metrics_configuration(
                    Bucket=s3_bucket_name,
                    Id='test'
                )
                print("Removed bucket metric")

            if bucket_object_change != None:
                response = admin_s3.delete_object(
                    Bucket=s3_bucket_name,
                    Key='test.txt'
                )
                print("Removed bucket object")

            if bucket_policy_change != None:
                response = admin_s3.put_bucket_policy(
                    Bucket=s3_bucket_name,
                    Policy=logging_bucket_policy['Policy']
                )
                print("Fixed bucket policy")
        if user:
            delete_test_user(user['UserName'])
            print("Deleted: " + user['UserName'])
        if trail_name:
            if trail_name_change != None:
                response = admin_cloudtrail.delete_trail(
                    Name=trail_name
                )
                print("Deleted CloudTrail: " + trail_name)
            if trail_tag_change != None:
                response = admin_cloudtrail.remove_tags(
                    ResourceId=trail_arn,
                    TagsList=[
                        {
                            'Key': 'string',
                            'Value': 'string'
                        },
                    ]
                )
                print("Fixed CloudTrail tagging")
            if trail_log_change != None:
                response = admin_cloudtrail.start_logging(
                    Name=trail_name
                )
                print("Fixed CloudTrail logging")
