"""
===================================================
test_admin_role_policy_rhedcloud_sns_protocol_block
===================================================

Type": "functional",
Name": "test_admin_role_policy_rhedcloud_sns_protocol_block",
Description": "Verify that the RHEDcloudSNSProtocolBlock statement within the RHEDcloudAdministratorRolePolicy policy is performing as expected against the specified resources.",
Plan": "Attach *:* and this policy, test each statement and expect AccessDenied errors for each blocked call.",
ExpectedResult": "Failure"

This test verifies that the RHEDcloudSNSProtocolBlock statement within the
RHEDcloudAdministratorRolePolicy are disallowing users from performing the
following actions on the appropriate resources and allowing on other resources:

sns:CreateTopic
sns:Subscribe

${testcount:8}

"""

from functools import partial

import boto3
import pytest

from aws_test_functions import (
    debug,
    get_account_number,
    has_status,
    ignore_errors,
    make_identifier,
    unexpected,
)

ACCESS_DENIED_NEEDLE = r"(AccessDenied|AuthorizationError)"


@pytest.fixture(scope="module")
def sns(session):
    return session.client("sns")


@pytest.dict_fixture
def shared_vars():
    return {
        "topic_arn": None,
        "queue_arn": None,
    }


def test_create_topic(sns, stack, shared_vars):
    """Verify access to create SNS topics.

    :param SNS.Client sns:
        Handle to the SNS API.
    :param contextlib.ExitStack stack:
        An existing context manager that will help clean up after ourselves.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    res = sns.create_topic(
        Name=make_identifier("test_sns_protocol_block"),
    )
    assert has_status(res, 200), unexpected(res)

    shared_vars["topic_arn"] = arn = res["TopicArn"]
    debug("Created SNS topic: {}".format(arn))

    stack.callback(
        ignore_errors(sns.delete_topic),
        TopicArn=arn,
    )


def test_create_queue(stack, shared_vars):
    """Verify access to create SQS queues.

    :param contextlib.ExitStack stack:
        An existing context manager that will help clean up after ourselves.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    name = make_identifier("test_sns_protocol_block")

    sqs = boto3.client("sqs")
    res = sqs.create_queue(
        QueueName=name,
    )
    assert has_status(res, 200), unexpected(res)

    url = res["QueueUrl"]
    debug("Created SQS queue: {}".format(url))
    shared_vars["queue_arn"] = "arn:aws:sqs:us-east-1:{}:{}".format(get_account_number(), name)

    stack.callback(
        ignore_errors(sqs.delete_queue),
        QueueUrl=url,
    )


permitted = partial(pytest.param, marks=pytest.mark.permitted)
blocked = partial(pytest.param, marks=pytest.mark.skip("moved to scp: test_sns.test_subscribe"))


@pytest.mark.parametrize("proto,endpoint", (
    permitted("https", "https://www.google.com"),
    permitted("sqs", "see-queue-arn-fixture"),
    blocked("http", "http://www.google.com"),
    permitted("email", "aws-admin-16@emory.edu"),
    permitted("email-json", "aws-admin-16@emory.edu"),
    blocked("sms", "4047277882"),
))
def test_subscribe(sns, topic_arn, queue_arn, proto, endpoint):
    """Verify access to subscribe to SNS endpoints.

    :param SNS.Client sns:
        Handle to the SNS API.
    :param contextlib.ExitStack stack:
        An existing context manager that will help clean up after ourselves.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    if proto == "sqs":
        endpoint = queue_arn

    debug("Attempting to subscribe: {}; {}".format(proto, endpoint))
    res = sns.subscribe(
        TopicArn=topic_arn,
        Protocol=proto,
        Endpoint=endpoint,
    )
    assert has_status(res, 200), unexpected(res)
