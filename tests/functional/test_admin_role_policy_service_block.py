"""
:Type: functional
:Name: admin_role_policy_service_block
:Description:
    Verify that the RHEDcloudServiceBlock statements within the
    RHEDcloudAdministratorRolePolicy policy is performing as expected against
    the specified resources.

:Plan:
    Attach *:* and this policy, test each statement and expect AccessDenied
    errors for each blocked call.

:ExpectedResult: "Failure"

${testcount:7}

"""

import pytest

from aws_test_functions import (
    has_status,
    unexpected,
)

# assume that each test will result in an AccessDenied error unless explicitly
# marked with a bypass
pytestmark = [
    pytest.mark.raises_access_denied,
]


@pytest.fixture
def ec2(session):
    return session.client("ec2")


@pytest.fixture
def ssm(session):
    return session.client("ssm")


@pytest.mark.skip("moved to scp: test_ssm.test_list_command_invocations")
def test_ssm_list_command_invocations(ssm):
    """Verify access to list SSM command invocations.

    :param SSM.Client ssm:
        A handle to the SSM API.

    :expected-result: Failure

    """

    res = ssm.list_command_invocations()
    assert has_status(res, 200), unexpected(res)


@pytest.mark.skip("moved to scp: test_ssm.test_list_commands")
def test_ssm_list_commands(ssm):
    """Verify access to list SSM commands.

    :param SSM.Client ssm:
        A handle to the SSM API.

    :expected-result: Failure

    """

    res = ssm.list_commands()
    assert has_status(res, 200), unexpected(res)


@pytest.mark.skip("moved to scp: test_ssm.test_list_documents")
def test_ssm_list_documents(ssm):
    """Verify access to list SSM documents.

    :param SSM.Client ssm:
        A handle to the SSM API.

    :expected-result: Failure

    """

    res = ssm.list_documents()
    assert has_status(res, 200), unexpected(res)


@pytest.mark.skip("moved to scp: test_ssm.test_send_command")
def test_ssm_send_command(ssm):
    """Verify access to send commands using SSM.

    :param SSM.Client ssm:
        A handle to the SSM API.

    :expected-result: Failure

    """

    res = ssm.send_command(
        DocumentName="AWS-RunShellScript",
        Targets=[{
            "Key": "tag:ServerRole",
            "Values": ["testing"],
        }],
    )
    assert has_status(res, 200), unexpected(res)


@pytest.mark.skip("moved to scp: test_vpc.test_delete_vpc")
def test_ec2_delete_vpc(ec2):
    """Verify access to delete VPCs.

    :param EC2.Client ec2:
        A handle to the EC2 API.

    :expected-result: Failure

    """

    res = ec2.delete_vpc(
        VpcId="vpc-1234",
        DryRun=True,
    )
    assert has_status(res, 200), unexpected(res)


@pytest.mark.bypass("raises_access_denied")
def test_ec2_describe_instances(ec2):
    """Verify access to describe EC2 instances.

    :param EC2.Client ec2:
        A handle to the EC2 API.

    :expected-result: Success

    """

    res = ec2.describe_instances()
    assert has_status(res, 200), unexpected(res)


@pytest.mark.bypass("raises_access_denied")
def test_ec2_describe_regions(ec2):
    """Verify access to describe EC2 regions.

    :param EC2.Client ec2:
        A handle to the EC2 API.

    :expected-result: Success

    """

    res = ec2.describe_regions()
    assert has_status(res, 200), unexpected(res)
