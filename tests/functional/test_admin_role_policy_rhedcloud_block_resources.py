'''
================================================
test_admin_role_policy_rhedcloud_block_resources
================================================

"Type": "functional",
"Name": "test_admin_role_policy_rhedcloud_block_resources",
"Description": "Verify that the rhedcloudBlockResources statement within the RHEDcloudAdministratorRolePolicy policy is performing as expected against the specified resources.",
"Plan": "Attach *:* and this policy, test each statement and expect AccessDenied errors for each blocked call.",
"ExpectedResult": "Failure"

This test verifies that the rhedcloudBlockResources statement is
disallowing users from performing the following actions on the
appropriate resources and allowing on other resources:

sns:create
sns:delete
sqs:create
sqs:add permissions
sqs:remove permissions
sqs:delete
lambda:create
lambda:invoke
lambda:delete
firehose:create
firehose:update
firehose:delete
kinesis:create
kinesis:describe
kinesis:delete


${testcount:45}

'''

import base64
import os

import boto3
import pytest

from aws_test_functions import (
    debug,
    get_account_number,
    has_status,
    ignore_errors,
    make_identifier,
    new_role,
    retry,
    unexpected,
)


ACCESS_DENIED_NEEDLE = r'.*(AccessDenied|Unauthorized|Forbidden|AuthorizationError).*'
EMPTY_ZIP_DATA = base64.b64decode(
    "UEsDBAoAAAAAADFYkEwAAAAAAAAAAAAAAAAFABAAdGVtcC9VWAwAl9jVWs261Fr56gPgUEsDBBQA"
    "CAAIADFYkEwAAAAAAAAAAAAAAAANABAAdGVtcC9pbmRleC5qc1VYDADNutRazbrUWvnqA+BLzs8r"
    "zs9J1cvJT9dQL0nNLVDX5AIAUEsHCJaN4lsWAAAAFAAAAFBLAQIVAwoAAAAAADFYkEwAAAAAAAAA"
    "AAAAAAAFAAwAAAAAAAAAAEDtQQAAAAB0ZW1wL1VYCACX2NVazbrUWlBLAQIVAxQACAAIADFYkEyW"
    "jeJbFgAAABQAAAANAAwAAAAAAAAAAECkgTMAAAB0ZW1wL2luZGV4LmpzVVgIAM261FrNutRaUEsF"
    "BgAAAAACAAIAhgAAAJQAAAAAAA=="
)
ADMIN_ID = make_identifier("rhedcloudtestrhedcloudblockresources")[:60]
USER_RHEDCLOUD_ID = make_identifier("rhedcloudtestrhedcloudblockresources")[:60]
USER_NON_RHECLOUD_ID = make_identifier("testrhedcloudblockresources")[:64]


# run a series of tests as admin or as a regular user, verifying access to
# interact with RHEDcloud and non-RHEDcloud resources
pytestmark = pytest.mark.parametrize("mode,name", (
    pytest.param("admin", ADMIN_ID, id="rhedcloud-as-admin"),
    pytest.param(
        "user", USER_RHEDCLOUD_ID,
        id="rhedcloud-as-user",
        marks=[
            pytest.mark.raises_access_denied,
            pytest.mark.skip("moved to scp"),
        ],
    ),
    pytest.param("user", USER_NON_RHECLOUD_ID, id="non-rhedcloud-as-user"),
))


@pytest.fixture(scope="module")
def role():
    """Create a new IAM Role for use with Lambda and Firehose.

    :returns:
        An IAM.Role resource.

    """

    services = [
        "edgelambda",
        "firehose",
        "lambda",
    ]

    with new_role("rhedcloudtestrhedcloudblockresources", services) as r:
        yield r


@pytest.fixture
def factory(session, mode):
    """Return the appropriate client factory based on the test mode.

    :param session:
        An authenticated user session.
    :param str mode:
        The AWS API client mode (admin or user).

    :returns:
        A client factory.

    """

    return session if mode == "user" else boto3


@pytest.fixture
def sns(factory):
    return factory.client("sns")


@pytest.fixture
def sqs(factory):
    return factory.client("sqs")


@pytest.fixture
def lambda_client(factory):
    return factory.client("lambda")


@pytest.fixture
def firehose(factory):
    return factory.client("firehose")


@pytest.fixture
def kinesis(factory):
    return factory.client("kinesis")


@pytest.dict_fixture
def shared_vars():
    return {
        "admin_topic_arn": None,
        "user_topic_arn": None,
        "admin_queue_url": None,
        "user_queue_url": None,
        "bucket_arn": "arn:aws:s3:::{}".format(os.environ["CLOUDTRAIL_BUCKET_NAME"])
    }


@pytest.fixture
def created_name(mode, name):
    """Return the ID of resources created by the admin user.

    This works on the assumption that the regular user is not permitted to
    create RHEDcloud resources, and therefore ``USER_RHEDCLOUD_ID`` will not
    exist for certain tests (like invoking a Lambda function).

    :param str mode:
        The AWS API client mode (admin or user).
    :param str name:
        The value used for various resource names.

    :returns:
        A string.

    """

    global ADMIN_ID, USER_RHEDCLOUD_ID

    if mode == "user" and name == USER_RHEDCLOUD_ID:
        name = ADMIN_ID

    return name


@pytest.fixture
def topic_arn(mode, name, admin_topic_arn, user_topic_arn):
    """Determine which SNS topic ARN to use based on the test mode.

    :param str mode:
        The AWS API client mode (admin or user).
    :param str name:
        The value used for various resource names.
    :param str admin_topic_arn:
        The value of the SNS topic ARN created by the admin user.
    :param str user_topic_arn:
        The value of the SNS topic ARN created by the normal user.

    :returns:
        A string.

    """

    arn = admin_topic_arn
    if mode == "user" and name.startswith("test"):
        arn = user_topic_arn

    return arn


@pytest.fixture
def queue_url(mode, name, admin_queue_url, user_queue_url):
    """Determine which SQS queue URL to use based on the test mode.

    :param str mode:
        The AWS API client mode (admin or user).
    :param str name:
        The value used for various resource names.
    :param str admin_topic_arn:
        The value of the SQS queue URL created by the admin user.
    :param str user_topic_arn:
        The value of the SQS queue URL created by the normal user.

    :returns:
        A string.

    """

    url = admin_queue_url
    if mode == "user" and name.startswith("test"):
        url = user_queue_url

    return url


# see also in SCP: test_sns.test_create_topic
def test_create_topic(sns, name, mode, stack, shared_vars):
    """Verify access to create SNS topics.

    :param SNS.Client sns:
        A handle to the SNS API.
    :param str name:
        Name of the topic to create.
    :param contextlib.ExitStack stack:
        An existing context manager that will help clean up after ourselves.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    res = sns.create_topic(
        Name=name,
    )
    assert has_status(res, 200), unexpected(res)

    shared_vars["{}_topic_arn".format(mode)] = arn = res["TopicArn"]
    debug("Created SNS topic: {}".format(arn))

    stack.callback(
        ignore_errors(test_delete_topic),
        sns,
        arn,
        name,
    )


# see also in SCP: test_sns.test_delete_topic
def test_delete_topic(sns, topic_arn, name):
    """Verify access to delete SNS topics.

    :param SNS.Client sns:
        A handle to the SNS API.
    :param str topic_arn:
        ARN of the SNS topic to delete.
    :param str name:
        Name of the topic to create.

    """

    debug("Attempting to delete topic: {}".format(topic_arn))
    res = sns.delete_topic(
        TopicArn=topic_arn,
    )
    assert has_status(res, 200), unexpected(res)


# see also in SCP: test_sqs.test_create_queue
def test_create_queue(sqs, name, stack, shared_vars, mode):
    """Verify access to create SQS topics.

    :param SQS.Client sqs:
        A handle to the SQS API.
    :param str name:
        Name of the queue to create.
    :param contextlib.ExitStack stack:
        An existing context manager that will help clean up after ourselves.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.
    :param str mode:
        The AWS API client mode (admin or user).

    """

    debug("Attempting to create queue: {}".format(name))
    res = sqs.create_queue(
        QueueName=name,
    )
    assert has_status(res, 200), unexpected(res)

    shared_vars["{}_queue_url".format(mode)] = url = res["QueueUrl"]
    debug("Created SQS queue: {}".format(url))

    stack.callback(
        ignore_errors(test_delete_queue),
        sqs,
        url,
        name,
    )


# see also in SCP: test_sqs.test_add_permission
def test_add_permission(sqs, queue_url, name):
    """Verify access to add permissions to SQS queues.

    :param SQS.Client sqs:
        A handle to the SQS API.
    :param str queue_url:
        URL of the SQS queue to modify.
    :param str name:
        Name of the queue to modify.

    """

    debug("Attempting to add permissions to queue: {} / {}".format(queue_url, name))
    res = sqs.add_permission(
        QueueUrl=queue_url,
        Label=name,
        AWSAccountIds=[get_account_number()],
        Actions=["GetQueueAttributes"],
    )
    assert has_status(res, 200), unexpected(res)


# see also in SCP: test_sqs.test_remove_permission
def test_remove_permission(sqs, queue_url, name):
    """Verify access to remove permissions from SQS queues.

    :param SQS.Client sqs:
        A handle to the SQS API.
    :param str queue_url:
        URL of the SQS queue to modify.
    :param str name:
        Name of the queue to modify.

    """

    debug("Attempting to remove permissions from queue: {} / {}".format(queue_url, name))
    res = sqs.remove_permission(
        QueueUrl=queue_url,
        Label=name,
    )
    assert has_status(res, 200), unexpected(res)


# see also in SCP: test_sqs.test_delete_queue
@pytest.mark.raises_access_denied_needle(r"(AccessDenied|NonExistentQueue)")
def test_delete_queue(sqs, queue_url, name):
    """Verify access to delete SQS queues.

    :param SQS.Client sqs:
        A handle to the SQS API.
    :param str queue_url:
        URL of the SQS queue to modify.
    :param str name:
        Name of the queue to delete.

    """

    if not queue_url.endswith(name):
        prefix = queue_url.rsplit("/", 1)[0]
        queue_url = "{}/{}".format(prefix, name)

    debug("Deleting SQS queue: {}".format(queue_url))
    res = sqs.delete_queue(
        QueueUrl=queue_url,
    )
    assert has_status(res, 200), unexpected(res)


# see also in SCP: test_lambda.test_create_function
def test_create_lambda_function(lambda_client, name, role, stack):
    """Verify access to create lambda functions.

    :param Lambda.Client lambda_client:
        A handle to the Lambda API.
    :param str name:
        Name of the function to create.
    :param IAM.Role role:
        An IAM Role resource to use when creating the lambda function.
    :param contextlib.ExitStack stack:
        An existing context manager that will help clean up after ourselves.

    """

    debug("Debug Lambda Role: {}".format(role.arn))
    res = retry(
        lambda_client.create_function,
        kwargs=dict(
            FunctionName=name,
            Runtime='nodejs12.x',
            Role=role.arn,
            Handler='index.js',
            Code={
                'ZipFile': EMPTY_ZIP_DATA,
            }
        ),
        msg="Attempting to create Lambda function",
        pred=lambda exc: "AccessDenied" not in str(exc),
        show=True,
    )
    assert has_status(res, 201), unexpected(res)

    debug("Created Lambda function: {}".format(name))

    stack.callback(
        ignore_errors(test_delete_lambda_function),
        lambda_client,
        name,
    )


# see also in SCP: test_lambda.test_invoke_function
def test_invoke(lambda_client, created_name):
    """Verify access to invoke lambda functions.

    :param Lambda.Client lambda_client:
        A handle to the Lambda API.
    :param str created_name:
        Name of the function to invoke.

    """

    res = lambda_client.invoke(
        FunctionName=created_name,
        InvocationType="DryRun",
    )
    assert has_status(res, 204), unexpected(res)


# see also in SCP: test_lambda.test_delete_function
def test_delete_lambda_function(lambda_client, name):
    """Verify access to delete lambda functions.

    :param Lambda.Client lambda_client:
        A handle to the Lambda API.
    :param str name:
        Name of the function to delete.

    """

    debug("Deleting Lambda function: {}".format(name))
    res = lambda_client.delete_function(
        FunctionName=name,
    )
    assert has_status(res, 204), unexpected(res)


# see also in SCP: test_kinesis.test_delivery_stream
def test_create_firehose(firehose, name, role, bucket_arn, stack):
    """Verify access to create Firehose streams.

    :param Firehose.Client firehose:
        A handle to the Firehose API.
    :param str name:
        Name of the stream to create.
    :param IAM.Role role:
        An IAM Role resource to use when creating the stream.
    :param str bucket_arn:
        ARN of an S3 bucket.
    :param contextlib.ExitStack stack:
        An existing context manager that will help clean up after ourselves.

    """

    res = retry(
        firehose.create_delivery_stream,
        kwargs=dict(
            DeliveryStreamName=name,
            ExtendedS3DestinationConfiguration={
                'RoleARN': role.arn,
                'BucketARN': bucket_arn,
            }
        ),
        msg="Attempting to create Firehose stream",
        pred=lambda exc: "AccessDenied" not in str(exc),
        show=True,
    )
    assert has_status(res, 200), unexpected(res)

    debug("Created Firehose: {}".format(name))

    stack.callback(
        ignore_errors(test_delete_firehose),
        firehose,
        name,
    )


# see also in SCP: test_kinesis.test_delivery_stream
def test_update_firehose(firehose, created_name):
    """Verify access to update Firehose streams.

    :param Firehose.Client firehose:
        A handle to the Firehose API.
    :param str created_name:
        Name of the stream to modify.

    """

    # wait for the stream to be active
    res = retry(
        firehose.describe_delivery_stream,
        kwargs=dict(
            DeliveryStreamName=created_name,
        ),
        pred=lambda exc: "explicit deny" not in str(exc),
        until=lambda res: res["DeliveryStreamDescription"]["DeliveryStreamStatus"] == "ACTIVE",
        msg="Checking firehose status...",
        show=True,
    )
    assert has_status(res, 200), unexpected(res)

    info = res["DeliveryStreamDescription"]
    assert len(info["Destinations"]) > 0

    dest = info["Destinations"][0]
    res = firehose.update_destination(
        DeliveryStreamName=created_name,
        CurrentDeliveryStreamVersionId=info["VersionId"],
        DestinationId=dest["DestinationId"],
        ExtendedS3DestinationUpdate={
            "RoleARN": dest["ExtendedS3DestinationDescription"]["RoleARN"],
            "BucketARN": dest["ExtendedS3DestinationDescription"]["BucketARN"]
        }
    )
    assert has_status(res, 200), unexpected(res)


# see also in SCP: test_kinesis.test_delivery_stream
def test_delete_firehose(firehose, name):
    """Verify access to delete Firehose streams.

    :param Firehose.Client firehose:
        A handle to the Firehose API.
    :param str name:
        Name of the stream to delete.

    """

    debug("Deleting Firehose: {}".format(name))
    res = firehose.delete_delivery_stream(
        DeliveryStreamName=name,
    )
    assert has_status(res, 200), unexpected(res)


# see also in SCP: test_kinesis.test_create_stream
def test_create_stream(kinesis, name, stack):
    """Verify access to create Kinesis streams.

    :param Kinesis.Client kinesis:
        A handle to the Kinesis API.
    :param str name:
        Name of the stream to create.
    :param contextlib.ExitStack stack:
        An existing context manager that will help clean up after ourselves.

    """

    res = kinesis.create_stream(
        StreamName=name,
        ShardCount=1
    )
    assert has_status(res, 200), unexpected(res)

    debug("Created Stream: {}".format(name))

    stack.callback(
        ignore_errors(test_delete_stream),
        kinesis,
        name,
    )


# see also in SCP: test_kinesis.test_describe_stream
def test_describe_stream(kinesis, created_name):
    """Verify access to describe Kinesis streams.

    :param Kinesis.Client kinesis:
        A handle to the Kinesis API.
    :param str created_name:
        Name of the stream to describe.

    """

    res = kinesis.describe_stream(
        StreamName=created_name,
    )
    assert has_status(res, 200), unexpected(res)


# see also in SCP: test_kinesis.test_delete_stream
def test_delete_stream(kinesis, name):
    """Verify access to delete Kinesis streams.

    :param Kinesis.Client kinesis:
        A handle to the Kinesis API.
    :param str name:
        Name of the stream to delete.

    """

    res = kinesis.delete_stream(
        StreamName=name,
    )
    assert has_status(res, 200), unexpected(res)
