'''
-----------------
test_vpcflowlog_role_policy.py
-----------------

"Type": "functional",
"Name": "test_vpcflowlog_role_policy",
"Description": "This test performs a series of positive and negative tests to verify the policy allows the specified CloudWatch logging functions..",
"Plan": "Create a test user, attach user to the RHEDcloudVpcFlowLogRolePolicy, and then perform positive tests for each allowed function and a set of negative tests that should not be allowed.",
"ExpectedResult": "Success"

This test verifies that the VpcFlowLogRole role's inline policy is
disallowing users from assuming this role and performing the following
CloudWatch operations:

"logs:CreateLogGroup",
"logs:CreateLogStream",
"logs:PutLogEvents",
"logs:DescribeLogGroups",
"logs:DescribeLogStreams"

Positive tests:
For these tests, create a user, attach

P1. Create user and attach RHEDcloudVpcFlowLogsPolicy and create a log group
P2. Using the same user with attached policy, create a log stream
P3. Using the same user with attached policy, put a log event
P4. Using the same user with attached policy, describe log groups
P5. Using the same user with attached policy, describe log streams

This test creates a user that must be cleaned up before the test finishes, regardless of whether the test passes or fails.

Negative test:
N1. Using the same user with attached policy, attempt to tag log group
N2. Using the same user with attached policy, attempt to delete log stream
N3. Using the same user with attached policy, attempt to delete log group

The above tests should all fail because these actions have not been explicitly allowed.

${testcount:8}

'''


import boto3
import time
from aws_test_functions import *
from botocore.exceptions import ClientError


def test_answer():


    user = create_test_user('test_vpcflowlog_role_policy')
    rhedcloudVpcFlowLogRolePolicyArn = "arn:aws:iam::"+get_account_number()+":policy/rhedcloud/RHEDcloudVpcFlowLogRolePolicy"
    attach_policy_to_user(user['UserName'],rhedcloudVpcFlowLogRolePolicyArn)
    session = create_session_for_test_user(user['UserName'])
    logsClient = session.client('logs')

    #Objects created for cleanup
    logGroup='testloggroup-'+get_uuid()
    logStream = 'testlogstream-'+get_uuid()
    timestamp_sec = int(time.time())
    timestamp_msec = timestamp_sec * 1000
    log_event = {'timestamp': timestamp_msec, 'message': 'Paul was here.'}

    #Clean-up items
    logGroupSet = False
    logStreamSet = False

    position = None
    try:

        #Positive tests
        try:
            position = '#P1: create a log group'
            logsClient.create_log_group(logGroupName=logGroup)
            logGroupSet = True

            position = '#P2: create a log stream'
            logsClient.create_log_stream(logGroupName=logGroup,logStreamName=logStream)
            logStreamSet = True

            position = '#P3: put a log event'
            logsClient.put_log_events(logGroupName=logGroup,logStreamName=logStream,logEvents=[log_event])

            position = '#P4: describe log groups'
            log_group_dict = logsClient.describe_log_groups(logGroupNamePrefix=logGroup)
            if (len(log_group_dict['logGroups']) != 1):
                assert False

            position = '#P5: describe log streams'
            log_stream_dict = logsClient.describe_log_streams(logGroupName=logGroup)
            if (len(log_stream_dict['logStreams']) != 1):
                assert False

        except ClientError as e:
            print (position,": FAILED")
            print ("Positive test failed with error: %s" % e)
            assert False

        #Negative Tests
        try:
            position = '#N1: tag a log group'
            logsClient.tag_log_group(logGroupName=logGroup,tags={'Purpose':'IAM Policy Test'})
            print (position,": FAILED")
            print ("Negative test failed with error: %s" % e)
            assert False

        except ClientError as e:
            if e.response['Error']['Code'] != 'AccessDeniedException':
               print (position,": Unexpected error: %s" % e)
               assert False

        try:
            position = '#N2: delete a log stream'
            logsClient.delete_log_stream(logGroupName=logGroup,logStreamName=logStream)
            print (position,": FAILED")
            print ("Positive test failed with error: %s" % e)
            assert False

        except ClientError as e:
            if e.response['Error']['Code'] != 'AccessDeniedException':
               print (position,": Unexpected error: %s" % e)
               assert False

        try:
            position = '#N3: delete a log group'
            logsClient.delete_log_group(logGroupName=logGroup)
            print (position,": FAILED")
            print ("Positive test failed with error: %s" % e)
            assert False

        except ClientError as e:
            if e.response['Error']['Code'] != 'AccessDeniedException':
               print (position,": Unexpected error: %s" % e)
               assert False

        assert True

    finally:
        #cleanup
        if logGroupSet or logStreamSet:
            try:
                priviledgedClient = boto3.client('logs')
                priviledgedClient.delete_log_group(logGroupName=logGroup)

            except ClientError as e:
                print (position,":cleanup: Can't delete log group: %s" % e)

        delete_test_user(user['UserName'])
