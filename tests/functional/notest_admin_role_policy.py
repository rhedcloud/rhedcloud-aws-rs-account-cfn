'''
-----------------
test_admin_role_policy.py
-----------------

Type": "functional",
Name": "test_admin_role_policy",
Description": "This test performs a series of positive and negative tests to verify iam access is denied for the four (4) roles listed in this policy and confirms appropriate iam actions are still allowed.",
Plan": "Create a test user, attach user to the RHEDcloudAdministratorRolePolicy, and then perform the following six tests that should be allowed: P1. Create a new role, P2. Attach a managed policy to the new role, P3. Detach the policy from the role, P4. Delete the role, P5. Create a managed policy, P6. Delete a managed policy. Then perform the following negative tests on each of the protected roles: N1. Delete the forbidden roles, N2. Attach a new managed policy to the roles, and N3. delete a managed policy that is attached to a forbidden role",
ExpectedResult": "Success"

4/4/18: Zach: This test is outdated due to advancements in IAM policies. Will need to review and update to new requirements

This test verifies that the RHEDcloudAdminRolePolicy role is
disallowing users from performing iam actions on these (forbidden) roles:

1."arn:aws:iam::*:role/RHEDcloudCentralAdministratorRole",
2."arn:aws:iam::*:role/RHEDcloudAdministratorRole"
3."arn:aws:iam::*:role/RHEDcloudSecurityRiskDetectionServiceRole"
4."arn:aws:iam::*:role/RHEDcloudAuditorRole"

Negative tests::
N1. Delete the roles
N2. Attach a new managed policy to the roles
N3. delete an managed policy that is attached to a forbidden role

Positive tests:

P1. Create a new role
P2. Attach a managed policy to the new role
P3. Detach the policy from the role
P4. Delete the role
P5. Create a managed policy
P6. Delete a managed policy

This test creates users, roles and policies that must be cleaned up.
${testcount:9}

'''

import boto3
from aws_test_functions import *
from botocore.exceptions import ClientError

def test_answer():
    #FYI: we've seen two occurrences of errors due to STS tokens not being considered valid
    #when we start performing operations on this session. May need to wait a bit or check for
    #validity if that problem occurs again.
    user = create_test_user('test_admin_role_policy')
    rhedcloudAdminRolePolicyArn = "arn:aws:iam::"+get_account_number()+":policy/RHEDcloudAdministratorRolePolicy"
    attach_policy_to_user(user['UserName'],rhedcloudAdminRolePolicyArn)
    session = create_session_for_test_user(user['UserName'])
    iamClient = session.client('iam')

    #Objects created for cleanup
    roleName='testrole-'+get_uuid()
    deleteRole = False
    detachRolePolicy = False
    newPolicyArn = None
    position = None
    try:

        #Positive tests
        try:
            position = '#P1:create a new role'
            arpDoc = """{
                            "Version": "2012-10-17",
                            "Statement": [
                              {
                                "Effect": "Allow",
                                "Principal": {
                                  "Service": "ec2.amazonaws.com",
                                  "AWS": "arn-goes-here"
                                },
                                "Action": "sts:AssumeRole"
                              }
                            ]
                          }"""
            arpDoc = arpDoc.replace("arn-goes-here",user['Arn'])
            iamClient.create_role(RoleName=roleName,AssumeRolePolicyDocument=arpDoc,Description='Created by CFN policy test script')
            deleteRole = True

            position = '#P2:attach a policy to the role'
            iamClient.attach_role_policy(RoleName=roleName,PolicyArn=rhedcloudAdminRolePolicyArn)
            detachRolePolicy = True

            position = '#P3:detach a policy'
            iamClient.detach_role_policy(RoleName=roleName,PolicyArn=rhedcloudAdminRolePolicyArn)
            detachRolePolicy = False

            position = '#P4:delete a role'
            iamClient.delete_role(RoleName=roleName)
            deleteRole = False

            position = '#P5:create a policy'
            newPolicyArn = createTestPolicyUsingClient(iamClient)['Policy']['Arn']

            position = '#P6:delete a policy'
            deleteTestPolicyUsingClient(iamClient,newPolicyArn)
            newPolicyArn = None


        except ClientError as e:
            print (position,": FAILED")
            print ("Positive test failed with error: %s" % e)
            assert False

        #Negative tests
        resource = session.resource('iam')
        forbiddenResources = ['RHEDcloudCentralAdministratorRole','RHEDcloudAdministratorRole','RHEDcloudSecurityRiskDetectionServiceRole','RHEDcloudAuditorRole','RHEDcloudVpcFlowLogRole']

        position = '#N1: delete forbidden resources'
        for forbidden in forbiddenResources:
           try:
               role = resource.Role(forbidden)
               role.delete()
               print(position,": Test failed. ",forbidden," role deleted.")
               assert False

           except ClientError as e:
               if e.response['Error']['Code'] != 'AccessDenied':
                   print ("Expected error.")
               elif e.response['Error']['Code'] != 'DeleteConflict':
                   print(position,": Test failed. ",forbidden," role was able to be deleted, but wasnt due to error.")
                   assert False

        position = '#N2: attach policy to forbidden roles'
        policyArn = createTestPolicyUsingClient(iamClient)['Policy']['Arn']

        try:

            for forbidden in forbiddenResources:
                iamClient.attach_role_policy(RoleName=forbidden,PolicyArn=policyArn)
                print(position,": Test failed. ",forbidden," policy attached to",forbidden," role.")
                iamClient.detach_role_policy(RoleName=forbidden,PolicyArn=policyArn)
                print("Policy detached")
                assert False

        except ClientError as e:
            if e.response['Error']['Code'] != 'AccessDenied':
               print (position,": Unexpected error: %s" % e)
               assert False

        finally:
            deleteTestPolicyUsingClient(iamClient,policyArn)

        position = '#N3: delete a policy that is attached to a forbidden role'
        try:
            iamClient.delete_policy(PolicyArn=rhedcloudAdminRolePolicyArn)

        except ClientError as e:
                if e.response['Error']['Code'] != 'AccessDenied':
                   print (position,": Unexpected error: %s" % e)
                   assert False

        position = '#test succeeds - cleanup then assert true'
        assert True

    finally:
        #cleanup so stack can be deleted if necessary
        if detachRolePolicy:
            position = '#try to detach the policy using the default session'
            try:
                boto3.client('iam').detach_role_policy(RoleName=roleName,PolicyArn=rhedcloudAdminRolePolicyArn)
                detachRolePolicy = False
            except ClientError as e:
                print (position,":cleanup: Can't detach policy from role: %s" % e)
        if deleteRole:
            if detachRolePolicy == False:
                position = '#try to delete the role using the default session'
                try:
                    boto3.client('iam').delete_role(RoleName=roleName)
                except ClientError as e:
                    print (position,":cleanup: Can't delete role: %s" % e)
        if newPolicyArn:
             #try to delete the policy using the default session
            try:
                deleteTestPolicyUsingClient(boto3.client('iam'),newPolicyArn)
            except ClientError as e:
                print (position,":cleanup: Can't delete policy %s" % e)


        delete_test_user(user['UserName'])
