import boto3
import json
'''
    pytest script validates the RHEDcloudAdministratorRolePolicy policy
    
    globals: 
        policySourceArn, string, ARN of the RHEDcloudAdministratorRolePolicy,
        deniedResources, list of strings, ARN of resources to simulate
        actionNames, list of strings, service and command name, e.g. "s3:CreateBucket", "iam:*", allows wildcards
        
    
        
'''
def get_account_number():
    client = boto3.client('sts')
    account_id = client.get_caller_identity()["Account"]
    return account_id
    
account = get_account_number()
policySourceArn="arn:aws:iam::"+account+":policy/RHEDcloudAdministratorRolePolicy"
deniedResources = ['arn:aws:iam::'+account+':role/RHEDcloudAdministratorRole',
                    'arn:aws:iam::'+account+':role/RHEDcloudCentralAdministratorRole',
                    'arn:aws:iam::'+account+':role/RHEDcloudSecurityRiskDetectionServiceRole',
                    'arn:aws:iam::'+account+':role/RHEDcloudAuditorRole',
                    'arn:aws:iam::'+account+':role/RHEDcloudVpcFlowLogRole']
actionNames = ['iam:*']

def simulate_managed_policy(policy_arn, actionNames, resourceArns):
    #see https://github.com/Robert-W/ReInvent-2016/blob/master/iam-policy-validate-powerfulActions.py
    iam = boto3.client('iam')
    # Retrieve the policy.
    get_policy_response = iam.get_policy(PolicyArn=policy_arn)
    default_version = get_policy_response["Policy"]["DefaultVersionId"]
    get_policy_version_response = iam.get_policy_version(PolicyArn=policy_arn, VersionId=default_version)
    policy_document = json.dumps(get_policy_version_response["PolicyVersion"]["Document"])
    
    # Simulate the policy
    simulation_response = iam.simulate_custom_policy(PolicyInputList=[policy_document], ActionNames=actionNames, ResourceArns=resourceArns)
    results = simulation_response['EvaluationResults']
    allows_powerful_action = False

    # Determine if any restricted actions are allowed.
    for actions in results:
        evalDecision = actions['EvalDecision']

        if(evalDecision == 'allowed'):
            actionName = actions['EvalActionName']
            #print ("Restricted action " + actionName + " was granted to resource " + policy_arn)
            allows_powerful_action = True

    # If any restricted actions were allowed, consider the resource non-compliant.
    if(allows_powerful_action):
        return "NON_COMPLIANT: "+actions['EvalResourceName']
    return "COMPLIANT: "+actions['EvalResourceName']

def test_answer():
    global policySourceArn, deniedResources, actionNames
    compliant = True
    for resource in deniedResources:
        resp = simulate_managed_policy(policySourceArn,actionNames, [resource])
        print(resp)
        if resp.startswith("NON_COMPLIANT"):
            compliant = False
        
    assert compliant
    
