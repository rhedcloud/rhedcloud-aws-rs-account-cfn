#!/usr/bin/env python
"""

This script is a placeholder for any steps necessary to cleanup resources
created for testing that may not get cleaned up by the tests which created the
resources.

"""


def main():
    pass


if __name__ == '__main__':
    main()
